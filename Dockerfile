FROM phusion/baseimage:latest

RUN apt-get update

### Ffmpeg
ADD bin/ffmpeg /usr/bin

### Rtmpmon
RUN mkdir -p /etc/service/rtmpmon
ADD rtmpmon /avs/rtmpmon
ADD run.sh /etc/service/rtmpmon/run

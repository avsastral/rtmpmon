#VERSION = $(shell git describe --tags || echo 0.6.3)
VERSION = $(echo 0.6.3)
SRC := $(wildcard *.go)
GOXENV = GOARCH=386 CGO_ENABLED=1
GOLINENV = GOOS=linux
GOMODOFF = GO111MODULE=off
GOFLAGS = -v -ldflags='-s -X main.version=$(VERSION)'

rtmpmon: $(SRC)
	go build $(GOFLAGS) -o $@

rtmpmon-lin: $(SRC)
	export GO111MODULE=auto && env $(GOLINENV) go build $(GOFLAGS) -o rtmpmon

rtmpmon32: $(SRC)
	env $(GOXENV) go install
	env $(GOXENV) go build $(GOFLAGS) -o $@

vendor::
	glide i
	go install


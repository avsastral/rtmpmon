package main

import (
	"database/sql"
	"fmt"
	"github.com/andviro/noodle"
	"github.com/andviro/noodle/bind"
	"github.com/andviro/noodle/render"
	"github.com/andviro/noodle/wok"
	"github.com/asdine/storm"
	"github.com/go-ini/ini"
	"github.com/golang/glog"
	_ "github.com/mattn/go-sqlite3"
	xContext "golang.org/x/net/context"
	"net/http"
	//"net"
)

type AppCloseHandler func() error
type AppCORSMiddleware func() noodle.Middleware

type App struct {
	r             *wok.Wok
	examToday     bool
	mwCORS        AppCORSMiddleware
	closeHandlers []AppCloseHandler
	bus           *Broker
	relay         *Relay
	pulls         *Pulls
	records       *Records
	pushes        *Pushes
	sys           *SysStats
	db            *sql.DB
	store         *storm.DB
	cfg           Config
	serviceWorker *ServiceWorker
	ready         chan struct{}
	port          string
	mediaDir      string
	logDir        string
}

type Config struct {
	ppe                         string
	regionalInfProcessingCenter bool
	ffmpegPullOverrideAgrs      map[string]string
	ffmpegPushOverrideAgrs      map[string]string
	region                      string
	rtmpServer                  string
	vpnServer                   string
	measureServer               string
	remotePublishServer         string
	publishServer               string
	avsMonitoringServer         string
	monitorServer               string
	publishCoreNumber           int
	publishBasePort             int
	publishPlayPort             int
	rtmpTrackBasePort           int
	rtmpExtraPorts              []int
	preferTCP                   bool
	preferPushTCP               bool
	pullBufSize                 string
	pushBufSize                 string
	pushMethod                  string
	recordSegmentSecs           int
	recordWallClock             bool
	recordResetTimestamps       bool
	newStreamNames              bool
	overrides                   map[string]string
}

func (app *App) loadConfig(configFile string) error {
	cfg, err := ini.Load(configFile)
	if err != nil {
		return err
	}
	c := cfg.Section("api_url_server")
	app.cfg.remotePublishServer = GetDomainFromUrl(c.Key("api_pull").MustString("video1.keydisk.ru"))

	c = cfg.Section("backend")
	app.cfg.measureServer = c.Key("measureServer").MustString("none")

	app.cfg.publishServer = c.Key("publishServer").MustString("video1.keydisk.ru")
	app.cfg.avsMonitoringServer = c.Key("avsMonitoringServer").MustString("avs-monitoring.sferainfo.net")
	app.cfg.publishBasePort = c.Key("publishBasePort").MustInt(5554)
	app.cfg.publishPlayPort = c.Key("publishPlayPort").MustInt(2935)
	app.cfg.publishCoreNumber = c.Key("publishCoreNumber").MustInt(1)
	app.cfg.newStreamNames = c.Key("newStreamNames").MustBool(false)

	app.cfg.rtmpServer = c.Key("rtmpServer").String()
	app.cfg.rtmpTrackBasePort = c.Key("rtmpTrackBasePort").MustInt(1112)
	app.cfg.rtmpExtraPorts = c.Key("rtmpExtraPorts").ValidInts(",")

	app.cfg.monitorServer = c.Key("monitorServer").String()

	app.cfg.preferTCP = c.Key("preferTCP").MustBool(false)
	app.cfg.preferPushTCP = c.Key("preferPushTCP").MustBool(true)
	app.cfg.pushMethod = c.Key("pushMethod").MustString("rtsp")

	app.cfg.pullBufSize = c.Key("pullBufSize").MustString("2048k")
	app.cfg.pushBufSize = c.Key("pushBufSize").MustString("2048k")

	app.cfg.recordSegmentSecs = c.Key("recordSegmentSecs").MustInt(1800)
	app.cfg.recordWallClock = c.Key("recordWallClock").MustBool(true)
	app.cfg.recordResetTimestamps = c.Key("recordResetTimestamps").MustBool(true)

	c = cfg.Section("ppe_number")
	app.cfg.region = c.Key("region").MustString("50")
	app.cfg.ppe = c.Key("ppe").MustString("Unknown")
	app.cfg.regionalInfProcessingCenter = c.Key("ripc").MustBool(false)

	c = cfg.Section("overrides")
	app.cfg.overrides = make(map[string]string)
	for k, v := range c.KeysHash() {
		key := fmt.Sprintf("%s_%s_%s", app.cfg.region, app.cfg.ppe, v)
		app.cfg.overrides[key] = k
	}

	c = cfg.Section("ffmpeg_pull")
	app.cfg.ffmpegPullOverrideAgrs = make(map[string]string)
	for k, v := range c.KeysHash() {
		app.cfg.ffmpegPullOverrideAgrs[k] = v
	}
	c = cfg.Section("ffmpeg_push")
	app.cfg.ffmpegPushOverrideAgrs = make(map[string]string)
	for k, v := range c.KeysHash() {
		app.cfg.ffmpegPushOverrideAgrs[k] = v
	}

	return nil
}

func (app *App) initDB(stateDbPath, dbPath string) error {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return err
	}
	app.db = db
	store, err := storm.Open(stateDbPath)
	if err != nil {
		return err
	}
	app.store = store
	return nil
}

func (app *App) OnClose(f AppCloseHandler) {
	glog.Infoln("application.Application.OnClose")
	app.closeHandlers = append(app.closeHandlers, f)
}

func AppFactory(port, mediaDir, logDir, configFile, stateDbPath, dbPath string) *App {
	glog.Infoln("AppFactory")
	app := &App{
		port:     port,
		mediaDir: mediaDir,
		logDir:   logDir,
		mwCORS:   MiddlewareCORS,
	}
	if err := app.loadConfig(configFile); err != nil {
		glog.Fatal("AppFactory: Error loading config file", err)
	}
	if err := app.initDB(stateDbPath, dbPath); err != nil {
		glog.Fatal("AppFactory: Error opening database", err)
	}
	app.r = wok.Default(JSONErrors, render.JSON)
	app.ready = make(chan struct{})
	defer close(app.ready)

	// Restore streams states
	defer app.restoreStates()
	app.registerPreflights()
	// Pull
	app.registerPulls()
	// Push
	app.registerPushes()
	// Record
	app.registerRecords()
	// Archive
	app.registerArchives()
	// System stats
	app.registerSysStats()
	// RtmpCli
	app.registerRtmpCli()
	// SSE
	app.registerSSE()
	// Service
	app.registerServiceWorker()
	// RTMP relaying
	app.registerRtmpRelay()

	return app
}

func (app *App) registerPreflights() {
	app.r.OPTIONS("/sys", app.mwCORS())(PreflightHandler)
	app.r.OPTIONS("/sys/exam", app.mwCORS())(PreflightHandler)
	app.r.OPTIONS("/sys/bw", app.mwCORS())(PreflightHandler)

	app.r.OPTIONS("/archive", app.mwCORS())(PreflightHandler)

	app.r.OPTIONS("/rec", app.mwCORS())(PreflightHandler)
	//app.r.OPTIONS("/rec/:streamName", app.mwCORS())(PreflightHandler)
	app.r.OPTIONS("/rec/del", app.mwCORS())(PreflightHandler)

	app.r.OPTIONS("/push", app.mwCORS())(PreflightHandler)
	//app.r.OPTIONS("/push/:streamName", app.mwCORS())(PreflightHandler)
	app.r.OPTIONS("/push/del", app.mwCORS())(PreflightHandler)

	app.r.OPTIONS("/pull", app.mwCORS())(PreflightHandler)
	//app.r.OPTIONS("/pull/:streamName", app.mwCORS())(PreflightHandler)
	app.r.OPTIONS("/pull/del", app.mwCORS())(PreflightHandler)
}

func PreflightHandler(c xContext.Context, w http.ResponseWriter, r *http.Request) error {
	// todo: правильно разобраться и настроить https://github.com/rs/cors
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.WriteHeader(http.StatusOK)
	//render.Yield(r, http.StatusOK, globals.EmptyJSON{})
	return nil
}

func MiddlewareCORS() noodle.Middleware {
	return func(next noodle.Handler) noodle.Handler {
		// type Handler func(context.Context, http.ResponseWriter, *http.Request) error
		return func(ctx xContext.Context, w http.ResponseWriter, r *http.Request) error {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

			return next(ctx, w, r)
		}
	}
}

func (app *App) restoreStates() {
	glog.Infoln("App.restoreStates")
	app.pulls.Restore()
	app.records.Restore()
}

func (app *App) registerRtmpRelay() {
	glog.Infoln("App.registerRtmpRelay")
	relay := Relay{}
	relay.Init(app)
	//app.r.GET("/publish")(relay.OnPublish)
	//app.r.GET("/done", pull.RLock)(pull.Restart)
}

func (app *App) registerSSE() {
	glog.Infoln("App.registerSSE")
	app.bus = NewBroker(app.pulls, app.records)
	app.r.GET("/events")(app.bus.SSE)
}

func (app *App) registerRtmpCli() {
	if app.cfg.rtmpServer != "" {
		// RtmpCli - только для инстанса на video1
		glog.Infoln("App.registerRtmpCli")
		rtsp := &RtmpCli{}
		rtsp.Init(app)
		rtspGrp := app.r.Group("/rtsp")
		{
			rtspGrp.GET("/")(rtsp.List)
		}
	}
}

func (app *App) registerSysStats() {
	glog.Infoln("App.registerSysStats")
	stats := SysStats{}
	stats.Init(app)
	statGrp := app.r.Group("/sys")
	{
		statGrp.GET("/exam")(stats.GetExam)
		statGrp.GET("/", stats.Lock)(stats.Get)
		statGrp.POST("/bw")(stats.MeasureBandwidth)
	}
}

func (app *App) registerArchives() {
	glog.Infoln("App.registerArchives")
	archive := Archive{}
	archive.Init(app)
	archiveGrp := app.r.Group("/archive")
	{
		archiveGrp.GET("/", archive.RLock)(archive.List)
	}
}

func (app *App) registerRecords() {
	glog.Infoln("App.registerRecords")
	record := Records{}
	record.Init(app)
	recGrp := app.r.Group("/rec")
	{
		recGrp.POST("/", record.Lock, bind.JSON(Record{}))(record.Add)
		recGrp.PUT("/", record.Lock, bind.JSON([]Record{}))(record.AddMany) //
		recGrp.GET("/", record.RLock)(record.List)
		recGrp.DELETE("/:streamName", record.Lock)(record.Delete)
		recGrp.POST("/del", record.Lock, bind.JSON([]string{}))(record.DeleteMany) // Массив идентификаторов
	}
}

func (app *App) registerPushes() {
	glog.Infoln("App.registerPushes")
	//push := Pushes{RtmpCli: rtmp}
	push := Pushes{}
	push.Init(app)
	pushGrp := app.r.Group("/push")
	{
		pushGrp.POST("/", push.Lock, bind.JSON(Push{}))(push.Add)
		pushGrp.PUT("/", push.Lock, bind.JSON([]Push{}))(push.AddMany) // Массив JSON-объектов
		pushGrp.GET("/", push.RLock)(push.List)
		pushGrp.DELETE("/:streamName", push.Lock)(push.Delete)
		pushGrp.POST("/del", push.Lock, bind.JSON([]string{}))(push.DeleteMany) // Массив идентификаторов
	}
}

func (app *App) registerPulls() {
	glog.Infoln("App.registerPulls")
	pull := Pulls{}
	pull.Init(app)
	pullGrp := app.r.Group("/pull")
	{
		pullGrp.POST("/", pull.Lock, bind.JSON(StreamDesc{}))(pull.Add)
		pullGrp.PUT("/", pull.Lock, bind.JSON([]StreamDesc{}))(pull.AddMany) // Массив JSON-объектов
		pullGrp.GET("/", pull.RLock)(pull.List)
		pullGrp.DELETE("/:streamName", pull.Lock)(pull.Delete)
		pullGrp.POST("/del", pull.Lock, bind.JSON([]string{}))(pull.DeleteMany) // Массив строк-идентификаторов
		//api.POST("/api/exit", n.Use(Bind(struct{ Exit string }{})).Then(rtmp.Exit))
	}
}

func (app *App) registerServiceWorker() {
	glog.Infoln("App.registerServiceWorker")
	app.serviceWorker = &ServiceWorker{}
	app.serviceWorker.Init(app)
}

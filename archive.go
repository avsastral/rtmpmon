package main

import (
	"github.com/golang/glog"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type RecInfo struct {
	Size   int64
	Modify int64
	Hash   string
}

type RecInfoMap map[string]RecInfo

type Archive struct {
	app *App
	Locker
}

func (archive *Archive) Init(app *App) {
	glog.Infoln("Archive.Init")
	archive.app = app
}

type ArchiveEntry struct {
	StreamName string      `json:"streamName"`
	Recordings []Recording `json:"recordings"`
}

type Recording struct {
	Date    int64  `json:"date"`
	PlayURI string `json:"playURI"`
	Valid   int    `json:"valid"`
}

func (ae *ArchiveEntry) ScanDates(recMap RecInfoMap, path, dateStr string, from, to *time.Time) {
	//glog.Infoln("Archive.ArchiveEntry.ScanDates")
	if ae.Recordings == nil {
		ae.Recordings = []Recording{}
	}
	_, offset := time.Now().Zone()
	res := make([]Recording, 0)
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		size := info.Size()
		modif := info.ModTime().Unix()
		components := strings.Split(path, "/")
		n := len(components)
		if n < 3 || !strings.HasSuffix(path, ".mp4") {
			return nil
		}
		rec := Recording{}
		d, err := time.Parse(RecDateFmt+" "+RecTimeFmt, dateStr+" "+strings.TrimSuffix(components[n-1], ".mp4"))
		if err != nil {
			glog.Warningf("Archive.ArchiveEntry.ScanDates.Walk: Failed to parse date for %s, error %v", path, err)
			return nil
		}
		d = d.Add(time.Duration(-offset) * time.Second)
		if (from != nil && d.Before(*from)) || (to != nil && d.After(*to)) {
			return nil
		}
		rec.Date = d.UTC().Unix()
		rec.PlayURI = filepath.Join(components[n-4:]...)
		recInfo, ok := recMap[rec.PlayURI]
		if !ok {
			rec.Valid = 0 // 0 - нет данных
		} else {
			if size == recInfo.Size && modif == recInfo.Modify {
				rec.Valid = 1 // 1 - валидно
			} else {
				rec.Valid = -1 // невалидно
			}
		}
		res = append(res, rec)
		return nil
	})
	if err != nil {
		glog.Warningf("Archive.ArchiveEntry.ScanDates.Walk: error scanning %s, error %v", path, err)
	}
	ae.Recordings = append(ae.Recordings, res...)
}

func (ae *ArchiveEntry) Filter(q string) (res bool) {
	//glog.Infoln("Archive.ArchiveEntry.Filter")
	q = strings.ToLower(q)
	if strings.Contains(strings.ToLower(ae.StreamName), q) {
		return true
	}
	b := ae.Recordings[:0]
	for _, r := range ae.Recordings {
		cleanUri := strings.Replace(r.PlayURI, "/", "", 0)
		cleanUri = strings.Replace(cleanUri, "-", "", 0)
		cleanUri = strings.TrimSuffix(cleanUri, ".mp4")
		if strings.Contains(strings.ToLower(cleanUri), q) {
			b = append(b, r)
			res = true
		}
	}
	ae.Recordings = b
	return
}

func (archive *Archive) list(q string, dateFrom, dateTo *time.Time) (res map[string]ArchiveEntry, err error) {
	glog.Infoln("Archive.list")
	res = make(map[string]ArchiveEntry)
	var recMap RecInfoMap
	yearDirs, err := ioutil.ReadDir(archive.app.mediaDir)
	// 1-получим данные из БД для проверки консистентности
	if recMap, err = archive.getDBRecInfo(); err != nil {
		glog.Errorf("SysStats.Cameras: Error accessing database: %v", err)
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	// на верхнем уровне директории архива должны лежать папочки по годам
	var entry ArchiveEntry
	var ok bool
	for _, yearDir := range yearDirs {
		if !checkYearDir(yearDir) {
			continue
		}
		dateDirs, err := ioutil.ReadDir(filepath.Join(archive.app.mediaDir, yearDir.Name()))
		if err != nil {
			return nil, err
		}
		for _, dateDir := range dateDirs {
			if !checkDateDir(dateDir) {
				continue
			}
			streamDirs, err := ioutil.ReadDir(filepath.Join(archive.app.mediaDir, yearDir.Name(), dateDir.Name()))
			if err != nil {
				return nil, err
			}
			for _, streamDir := range streamDirs {
				streamNameParts := strings.Split(streamDir.Name(), " ")
				// выберем уже что-то что есть, либо создадим новую запись
				if entry, ok = res[streamNameParts[0]]; !ok {
					entry = ArchiveEntry{StreamName: streamNameParts[0], Recordings: []Recording{}}
				}
				entry.ScanDates(recMap, filepath.Join(archive.app.mediaDir, yearDir.Name(), dateDir.Name(), streamDir.Name()), dateDir.Name(), dateFrom, dateTo)
				if entry.Filter(q) {
					res[entry.StreamName] = entry
				}
			}
		}
	}
	return res, nil
}

func (archive *Archive) getDBRecInfo() (RecInfoMap, error) {
	glog.Infoln("SysStats.Cameras")
	res := make(RecInfoMap)
	db := archive.app.db

	rows, err := db.Query(`select f_path, f_size, f_modify, f_hash from cms_records`)
	if err != nil {
		glog.Errorf("SysStats.Cameras: Error accessing database: %v", err)
		return nil, err
	}
	defer rows.Close()

	var temp RecInfo
	var fPath string
	for rows.Next() {
		if err = rows.Scan(&fPath, &temp.Size, &temp.Modify, &temp.Hash); err != nil {
			glog.Errorf("SysStats.Cameras: Error scanning database: %v", err)
			return nil, err
		}
		res[fPath] = temp
	}
	if err = rows.Err(); err != nil {
		glog.Errorf("SysStats.Cameras: Error after scanning database: %v", err)
	}
	return res, err

}

func getTimeFromQueryParam(param string) *time.Time {
	glog.Infoln("Archive.getTimeFromQueryParam")
	if param == "" {
		return nil
	}
	date, err := strconv.ParseInt(param, 10, 64)
	if err == nil {
		d := time.Unix(date, 0)
		return &d
	}
	return nil
}

func checkYearDir(info os.FileInfo) bool {
	glog.Infoln("Archive.checkYearDir")
	if !info.IsDir() {
		return false
	}
	if year, err := strconv.Atoi(info.Name()); err != nil {
		return false
	} else {
		if year < 2016 || year > time.Now().Year()+1 {
			// папка похожа на какой-то левак
			return false
		}
	}
	return true
}

func checkDateDir(info os.FileInfo) bool {
	glog.Infoln("Archive.checkDateDir")
	if !info.IsDir() {
		return false
	}
	_, err := time.Parse(RecDateFmt, info.Name())
	if err != nil {
		return false
	}
	return true
}

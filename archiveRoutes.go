package main

import (
	"github.com/andviro/noodle/render"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
	"strings"
)

func (archive *Archive) List(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Archive.Route.List")
	q := strings.TrimSpace(r.URL.Query().Get("q"))
	dateFrom := getTimeFromQueryParam(strings.TrimSpace(r.URL.Query().Get("from")))
	dateTo := getTimeFromQueryParam(strings.TrimSpace(r.URL.Query().Get("to")))

	if res, err := archive.list(q, dateFrom, dateTo); err != nil {
		w.WriteHeader(500)
		return err
	} else {
		return render.Yield(c, 200, res)
	}
}

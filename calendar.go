package main

import (
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type CalendarStreamInfo struct {
	Ppe          string `json:"ppe"`
	CamId        int    `json:"cam_id"`
	AudienceName string `json:"audience_name"`
	AudienceCode string `json:"audience_code"`
	RemoteUrl    string `json:"remote_url"`
	PlayUrl      string `json:"play_url"`
	RecUrl       string `json:"rec_url"`
	RtmpUrl      string `json:"rtmp_url"`
	RtspUrl      string `json:"rtsp_url"`
}

type StreamList []CalendarStreamInfo


func getCalendarData(avsMonitoringServer, ppe string) (streams StreamList, err error) {
	glog.Infoln("Calendar.getCalendarData")
	var location *time.Location
	// todo: а если не москва?
	location, err = time.LoadLocation("Europe/Moscow")
	if err != nil {
		glog.Errorf("calendar.getCalendarData :: ошибка получения данных календаря экзаменов: %s\n", err)
		return nil, err
	}
	var now = time.Now()
	var fBegin = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, location)
	var fEnd = time.Date(now.Year(), now.Month(), now.Day(), 23, 59, 59, 0, location)
	var filters = url.QueryEscape(fmt.Sprintf(`[{"field_name":"ppe","field_value":"%s"},{"field_name":"date","field_value":"%d - %d"}]`, ppe, fBegin.Unix(), fEnd.Unix()))
	res, err := http.Get(fmt.Sprintf("https://%s/api/calendar/list?token=internal-api-token&filters=%s", avsMonitoringServer, filters))
	if err != nil {
		glog.Errorf("calendar.getCalendarData :: ошибка получения данных календаря экзаменов: %s\n", err)
		return nil, err
	}
	rawData, err := ioutil.ReadAll(res.Body)
	if err = res.Body.Close(); err != nil {
		glog.Errorf("calendar.getCalendarData :: ошибка чтения данных календаря экзаменов: %s\n", err)
		return nil, err
	}
	if err := json.Unmarshal(rawData, &streams); err != nil {
		glog.Errorf("calendar.getCalendarData :: ошибка распаковки данных календаря экзаменов: %s\n", err)
		return nil, err
	}
	return streams, nil
}

package main

const (
	SILENT  = 0
	ERROR   = 1
	WARN    = 2
	INFO    = 3
	DEBUG   = 4
	PROFILE = 5
)

const (
	BadRecNoFile        = 100
	BadRecManyFiles     = 101
	BadRecIncorrectTime = 102
	BadRecErrorReadDir  = 103
)

//var PushPorts = []string{"12935", "12936", "12937", "12938", "15554", "15556", "15556", "15557"}

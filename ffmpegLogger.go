package main

import (
	"io"
	"io/ioutil"
	"regexp"
	"strings"
	"sync"
)

var videoStreamRe = regexp.MustCompile(`Stream #[0-9]+:[0-9]+: Video: ([^,]*)`)
var videoStreamRe2 = regexp.MustCompile(`([0-9]+x[0-9]+)[^,]*, ([0-9]+)`)
var audioStreamRe = regexp.MustCompile(`Stream #[0-9]+:[0-9]+: Audio: ([^,]*), ([^,]*), ([^,]*)`)
var recSegmentRe = regexp.MustCompile(`(?m)^\[segment.*('.*').*for writing$`)

const StreamCheckLogSizeLimit = 1024
const StreamNonMonotonousThreshold = 64

const (
	FFmpegNoErrors          = 0
	FFmpegStuck             = 1
	FFmpegNonMonotonousDTS  = 2
	FFmpegSTInvalidDropping = 3
	FFmpegAlreadyPublishing = 4
	FFmpegPossibleNoVideo   = 5
)

type StreamInfo struct {
	Video VideoInfo
	Audio AudioInfo
}

type VideoInfo struct {
	Codec      string
	Resolution string
	Fps        string
}

type AudioInfo struct {
	Codec string
	Rate  string
	Aural string
}

type FfmpegLogParser struct {
	buf              [1024]byte
	checkLog         []string
	checkMu          sync.RWMutex
	pos              int
	headerParsed     bool
	videoParsed      bool
	audioParsed      bool
	AlreadyPublished bool
	RecSegmentChan   chan string
	w                io.Writer
	s                StreamInfo
	Res              chan StreamInfo
}

func NewFfmpegLogParser(w io.Writer) *FfmpegLogParser {
	if w == nil {
		w = ioutil.Discard
	}
	return &FfmpegLogParser{
		w:              w,
		Res:            make(chan StreamInfo, 1),
		checkLog:       make([]string, 0, StreamCheckLogSizeLimit),
		RecSegmentChan: make(chan string, 1),
	}
}

func (fp *FfmpegLogParser) Cleanup() {
	close(fp.Res)
	close(fp.RecSegmentChan)
}

func (fp *FfmpegLogParser) HealthCheck() uint64 {
	fp.checkMu.Lock()
	defer func() {
		defer fp.checkMu.Unlock()
		fp.checkLog = make([]string, 0, StreamCheckLogSizeLimit)
	}()

	if len(fp.checkLog) == 0 {
		return FFmpegStuck
	}
	nonMonotonousCnt := 0
	var hasFrames bool
	for _, line := range fp.checkLog {
		if strings.HasPrefix(line, "frame=") && !strings.Contains(line, "fps=0.0") {
			// --- лог здорового потока
			// frame=   42 fps=0.0 q=-1.0 size=N/A time=00:00:01.64 bitrate=N/A speed=3.13x
			// frame=   55 fps= 52 q=-1.0 size=N/A time=00:00:02.16 bitrate=N/A speed=2.06x
			// --- лог потока умирающей камеры
			// frame=    0 fps=0.0 q=-1.0 size=N/A time=00:00:01.40 bitrate=N/A speed=2.66x
			// frame=    0 fps=0.0 q=-1.0 size=N/A time=00:00:01.92 bitrate=N/A speed=1.85x
			hasFrames = true
			continue
		}
		if strings.Contains(line, "Non-monotonous DTS in output stream") {
			nonMonotonousCnt++
			if nonMonotonousCnt > StreamNonMonotonousThreshold {
				return FFmpegNonMonotonousDTS
			}
			continue
		}
		if strings.Contains(line, "invalid dropping st") {
			return FFmpegSTInvalidDropping
		}
	}
	if !hasFrames {
		return FFmpegPossibleNoVideo
	}

	return FFmpegNoErrors
}

func (fp *FfmpegLogParser) Write(p []byte) (n int, err error) {
	lines := strings.Split(string(p), "\n")
	// 1-чекнем на предмет
	// [segment @ 0x13b904910] Opening '2022/2022-12-15/1927_39 (50_1927_1000)/17-41-27.mp4' for writing
	// [segment @ 0x6b0ae80] Opening '2022/2022-12-20/055555_1 (Камера ТВ)/16-01-20.mp4' for writing
	// оно идет после Input и до Output
	for _, line := range lines {
		if !strings.HasPrefix(line, "[segment @") {
			continue
		}
		matches := recSegmentRe.FindAllStringSubmatch(line, -1)
		if len(matches) == 1 {
			groups := matches[0]
			if len(groups) == 2 {
				s := strings.TrimPrefix(groups[1], "'")
				s = strings.TrimSuffix(s, "'")
				fp.RecSegmentChan <- s
				//select {
				//case fp.RecSegmentChan <- s:
				//case <-time.After(500 * time.Millisecond):
				//}
			}
		}
	}

	// 2-уже наши штатные дела по helthcheck
	if fp.headerParsed {
		if len(fp.checkLog) < StreamCheckLogSizeLimit {
			func() {
				fp.checkMu.Lock()
				defer fp.checkMu.Unlock()
				// по отдельным логам видно, что ошибка может возникать после того как будет распаршены заголовки,
				// поэтому тут тоже проверим а не AlreadyPublished ли часом наш поток
				for _, line := range lines {
					if strings.HasPrefix(line, "==> AVS") {
						// пропускаем "наши" буквы, нас интересуют исключительно буквы ffmpeg-а
						continue
					}
					if strings.Contains(line, "Server error: Already publishing") {
						fp.AlreadyPublished = true
					}
					fp.checkLog = append(fp.checkLog, strings.TrimSpace(line))
				}
			}()
		}
		return fp.w.Write(p)
	}

	for _, b := range p {
		if b == '\r' || b == '\n' || fp.pos >= len(fp.buf) {
			fp.ProcessLine(string(fp.buf[:fp.pos]))
			fp.pos = 0
			continue
		}
		fp.buf[fp.pos] = b
		fp.pos++
	}
	return fp.w.Write(p)
}

func (fp *FfmpegLogParser) ProcessLine(s string) {
	// todo: улучшить парсинг - чтобы иметь представление о параметрах входного и выходного потоков
	s = strings.TrimSpace(s)
	if strings.Contains(s, "Server error: Already publishing") {
		fp.AlreadyPublished = true
	}
	if strings.HasPrefix(s, "frame=") || strings.HasPrefix(s, "size=") {
		fp.headerParsed = true
		fp.Res <- fp.s
		return
	}
	if !fp.videoParsed && videoStreamRe.MatchString(s) {
		vdata := videoStreamRe.FindAllStringSubmatch(s, -1)
		if len(vdata) > 0 && len(vdata[0]) > 1 {
			fp.s.Video.Codec = vdata[0][1]
		}
		vdata = videoStreamRe2.FindAllStringSubmatch(s, -1)
		if len(vdata) > 0 && len(vdata[0]) > 2 {
			fp.s.Video.Resolution = vdata[0][1]
			fp.s.Video.Fps = vdata[0][2]
		}
		fp.videoParsed = true
	}
	if !fp.audioParsed && audioStreamRe.MatchString(s) {
		adata := audioStreamRe.FindAllStringSubmatch(s, -1)
		if len(adata) > 0 && len(adata[0]) > 3 {
			fp.s.Audio.Codec = adata[0][1]
			fp.s.Audio.Rate = adata[0][2]
			fp.s.Audio.Aural = adata[0][3]
			fp.audioParsed = true
		}
	}
	if fp.videoParsed && fp.audioParsed {
		fp.headerParsed = true
		fp.Res <- fp.s
		return
	}
}

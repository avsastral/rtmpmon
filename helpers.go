package main

import (
	"encoding/json"
	"fmt"
	"github.com/andviro/noodle"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

func UrlJoin(base string, components ...string) string {
	base = strings.TrimRight(base, "/")
	for _, s := range components {
		base = base + "/" + strings.TrimLeft(s, "/")
	}
	return base
}

func JSONErrors(next noodle.Handler) noodle.Handler {
	return func(c context.Context, w http.ResponseWriter, r *http.Request) error {
		err := next(c, w, r)
		if err != nil {
			json.NewEncoder(w).Encode(M{"error": err.Error(), "data": err})
		}
		return err
	}
}

type Locker struct {
	mu sync.RWMutex
}

func (p *Locker) Lock(next noodle.Handler) noodle.Handler {
	return func(c context.Context, w http.ResponseWriter, r *http.Request) error {
		p.mu.Lock()
		defer p.mu.Unlock()
		return next(c, w, r)
	}
}

func (p *Locker) RLock(next noodle.Handler) noodle.Handler {
	return func(c context.Context, w http.ResponseWriter, r *http.Request) error {
		p.mu.RLock()
		defer p.mu.RUnlock()
		return next(c, w, r)
	}
}


func CheckPortAvailability(domain, port string) (res bool) {
	addr := fmt.Sprintf("%s:%s", domain, port)
	conn, err := net.DialTimeout("tcp", addr, portCheckTimeout*time.Second)
	if err != nil {
		glog.Infof("CheckPortAvailability :: check failed with error: %s\n", err)
		res = false
		return
	}
	defer conn.Close()
	res = true
	return
}

func CheckUrlAvailability(url string) bool {
	client := http.Client{Timeout: measureTimeout}

	if _, err := client.Get(url); err != nil {
		return false
	}
	return true
}

func GetDomainFromUrl(urlAddr string) (domain string) {
	glog.Infoln("GetDomainFromUrl")
	domain = ""
	if u, err := url.Parse(urlAddr); err != nil {
		return
	} else {
		domain = u.Hostname()
	}
	return
}

func GetFfmpegArg(overrides map[string]string, name, defVal string) string {
	glog.Infoln("helpers.GetFfmpegArg")
	if override, ok := overrides[name]; ok {
		glog.Infof("helpers.GetFfmpegArg: Override ffmpeg arg %s from %s to %s", name, defVal, override)
		return override
	}
	glog.Infof("helpers.GetFfmpegArg: No overrides ffmpeg arg %s return defaults %s", name, defVal)
	return defVal
}
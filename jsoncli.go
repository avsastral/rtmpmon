package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"math/rand"
	"net"
	"os"
	"regexp"
	"sync"
	"time"

	"io/ioutil"
	"net/http"
	"runtime/debug"
	"strings"
)

// todo: тихий ужас - отрефакторить
const (
	updateInterval = 10000
	updateTimeout  = 3
	saveInterval   = 60 * 10
)

type StreamState map[string]Stream

type ListResult struct {
	Results        StreamState `json:"results"`
	NumResults     int         `json:"num_results"`
	Page           int         `json:"page"`
	TotalPages     int         `json:"total_pages"`
	ResultsPerPage int         `json:"results_per_page"`
}

type Track struct {
	Codec               string `json:"codec"`
	BytesCount          int64  `json:"bytesCount"`
	PacketsCount        int    `json:"packetsCount"`
	DroppedBytesCount   int64  `json:"droppedBytesCount"`
	DroppedPacketsCount int    `json:"droppedPacketsCount"`
}

type Pull struct {
	LocalStreamName string `json:"localStreamName"`
	Uri             string `json:"uri"`
}

type Stream struct {
	Name              string    `json:"name"`
	Type              string    `json:"type"`
	Online            bool      `json:"online"`
	LastSeen          time.Time `json:"lastSeen"`
	Ip                string    `json:"ip"`
	Port              int       `json:"port"`
	PlayPort          int       `json:"playPort"`
	FarIp             string    `json:"farIp"`
	FarPort           int       `json:"farPort"`
	CreationTimestamp float64   `json:"creationTimestamp"`
	UpTime            float64   `json:"upTime"`
	Appname           string    `json:"appname"`
	Title             string    `json:"title,omitempty"`   // optional
	Comment           string    `json:"comment,omitempty"` // optional
	Audio             Track     `json:"audio"`
	Video             Track     `json:"video"`
	PullSettings      Pull      `json:"pullSettings"`
}

type RtmpCli struct {
	app    *App
	states map[string]StreamState
	sync.RWMutex
	outAddr string
}

type RequestResult struct {
	Port string
	Data *StreamState
}

type M map[string]interface{}

type Req struct {
	Cmd  string
	Args M
}

type Rep struct {
	Data        interface{} `json:"data"`
	Description string      `json:"description"`
	Status      string      `json:"status"`
}

var validStreamRe = regexp.MustCompile("[0-9]+_[0-9]+(_[0-9]{4}(_[0-9]+)?)?|[0-9a-fA-F]{40}_[0-9]+")

func init() {
	rand.Seed(time.Now().Unix())
}

func getHostIp() (res string) {
	glog.Infoln("RtmpCli.getHostIp")
	res = "127.0.0.1"
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		glog.Errorf("JsonCLI: Error getting local address: %v", err)
		return
	}
	for _, a := range addrs {
		if ipaddr, ok := a.(*net.IPNet); ok {
			ip := ipaddr.IP
			ip4 := ip.To4()
			if !ip.IsLoopback() && ip4 != nil && ip4[0] != 192 && ip4[0] != 172 {
				res = ip4.String()
				break
			}
		}
	}
	return
}

func (req Req) Send(conn net.Conn) error {
	glog.Infoln("RtmpCli.Req.Send")
	if _, err := fmt.Fprintf(conn, "%s ", req.Cmd); err != nil {
		return err
	}
	for k, v := range req.Args {
		if _, err := fmt.Fprintf(conn, "%s=%v ", k, v); err != nil {
			return err
		}
	}
	if _, err := fmt.Fprintf(conn, "\r\n"); err != nil {
		return err
	}
	return nil
}

// специфично для Тулы
func (rtmpd *RtmpCli) sendMonitoringState() {
	glog.Infoln("RtmpCli.sendMonitoringState")
	rtmpd.RLock()
	defer rtmpd.RUnlock()

	uri := rtmpd.app.cfg.monitorServer
	if uri == "" {
		return
	}
	if !strings.HasSuffix(uri, "/") {
		uri += "/"
	}

	client := &http.Client{Timeout: 2 * time.Second}
	var wg sync.WaitGroup
	for port, streamData := range rtmpd.states {
		buf := new(bytes.Buffer)
		if err := json.NewEncoder(buf).Encode(streamData); err != nil {
			glog.Errorf("RtmpCli.sendMonitoringState: Error encoding streams for port %s: %v", port, err)
			continue
		}
		wg.Add(1)
		go func(port string) {
			defer wg.Done()

			_, err := client.Post(uri, "application/json", buf)
			if err != nil {
				glog.Errorf("RtmpCli.sendMonitoringState: Error sending monitoring for port %s: %v", port, err)
				return
			}
			glog.Infof("RtmpCli.sendMonitoringState: Sent streams for port %s to %s", port, uri)
		}(port)
	}
	wg.Wait()
}

func errorRecovery() {
	glog.Infoln("RtmpCli.errorRecovery")
	err := recover()
	if err != nil {
		glog.Errorf("panic: %v\n%s", err, string(debug.Stack()))
	}
}

func (rtmpd *RtmpCli) requestState(port string) *StreamState {
	glog.Infoln("RtmpCli.requestState")
	defer errorRecovery()

	addr := fmt.Sprintf("%s:%s", rtmpd.app.cfg.rtmpServer, port)
	conn, err := net.DialTimeout("tcp", addr, updateTimeout*time.Second)
	if err != nil {
		glog.Errorf("RtmpCli.requestState: Error connecting to %s: %v", addr, err)
		return nil
	}
	defer conn.Close()

	var res StreamState
	if err := rtmpd.reqRep(conn, Req{Cmd: "list"}, &res); err != nil {
		glog.Errorf("RtmpCli.requestState: Error getting info from %s: %v", addr, err)
		return nil
	}
	return &res
}

func (rtmpd *RtmpCli) update() {
	glog.Infoln("RtmpCli.update")
	var wg sync.WaitGroup
	results := make(chan RequestResult)
	// main cores
	for i := 0; i < rtmpd.app.cfg.publishCoreNumber; i++ {
		wg.Add(1)
		go func(port string) {
			defer wg.Done()
			results <- RequestResult{
				Port: port,
				Data: rtmpd.requestState(port),
			}
		}(fmt.Sprint(rtmpd.app.cfg.rtmpTrackBasePort + i))
	}
	// extra ports
	for _, extraPort := range rtmpd.app.cfg.rtmpExtraPorts {
		wg.Add(1)
		go func(port string) {
			defer wg.Done()
			results <- RequestResult{
				Port: port,
				Data: rtmpd.requestState(port),
			}
		}(fmt.Sprint(extraPort))
	}
	go func() {
		wg.Wait()
		close(results)
	}()

	// parse results
	for res := range results {
		if res.Data == nil { // there was an error
			glog.Errorf("RtmpCli.update: Skipping results from port %s", res.Port)
			continue
		}
		func() {
			rtmpd.Lock()
			defer rtmpd.Unlock()

			serverState, ok := rtmpd.states[res.Port]
			if !ok {
				serverState = make(StreamState)
			} else {
				for key, stream := range serverState {
					stream.Online = false
					serverState[key] = stream
				}
			}
			var offset int
			cameras := rtmpd.app.sys.Cameras()
			n := 0
			for key, stream := range *res.Data {
				if stream.Port < 8000 {
					if stream.Port < 5000 {
						// RTMP push
						offset = stream.Port - rtmpd.app.cfg.publishPlayPort
					} else {
						// RTSP push
						offset = stream.Port - rtmpd.app.cfg.publishBasePort
					}
					stream.Port = rtmpd.app.cfg.publishBasePort + offset
					stream.PlayPort = rtmpd.app.cfg.publishPlayPort + offset
				} else {
					stream.Port = 8554
					stream.PlayPort = 8935
				}

				stream.Online = true
				stream.LastSeen = time.Now()
				if stream.Ip == "127.0.0.1" {
					stream.Ip = rtmpd.outAddr
				}
				// обновление заголовков потоков
				// У Тулы имена потоков единообразные, так что можно и так
				if camDesc, ok := cameras[key]; ok {
					stream.Title = camDesc.Name
					stream.Comment = camDesc.Comment
				}
				serverState[key] = stream
				n++
			}
			rtmpd.states[res.Port] = serverState
			glog.Infof("RtmpCli.update: Collected %d streams from port %s", n, res.Port)
		}()
	}
}

func (rtmpd *RtmpCli) PeriodicUpdate() {
	glog.Infoln("RtmpCli.PeriodicUpdate")
	for {
		rtmpd.update()
		rtmpd.sendMonitoringState()
		if err := rtmpd.Save(); err != nil {
			glog.Errorf("RtmpCli.PeriodicUpdate: Error saving state: %v", err)
		}
		time.Sleep(time.Duration(updateInterval/2+rand.Int31n(updateInterval/2)) * time.Millisecond)
	}
}

func (rtmpd *RtmpCli) PeriodicSave() {
	glog.Infoln("RtmpCli.PeriodicSave")
	for {
		if err := rtmpd.Save(); err != nil {
			glog.Errorf("RtmpCli.PeriodicSave: Error saving state: %v", err)
		}
		time.Sleep(saveInterval * time.Second)
	}
}

func (rtmpd *RtmpCli) filterStreams(states map[string]StreamState) map[string]StreamState {
	glog.Infoln("RtmpCli.filterStreams")
	res := make(map[string]StreamState)
	for k, v := range states {
		tres := make(StreamState)
		for streamName, streamDesc := range v {
			if !validStreamRe.MatchString(streamName) {
				continue
			}
			tres[streamName] = streamDesc
		}
		res[k] = tres
	}
	return res
}

func (rtmpd *RtmpCli) filterStreamsByPPE(states map[string]StreamState, ppe string) map[string]StreamState {
	glog.Infoln("RtmpCli.filterStreamsByPPE: ", ppe)
	res := make(map[string]StreamState)
	for k, v := range states {
		tres := make(StreamState)
		for streamName, streamDesc := range v {
			// todo: check for correct
			parts := strings.Split(streamName, "_")

			index := 0
			if len(parts) > 2 {
				index = 1
			}
			if ppe != parts[index] {
				continue
			}
			tres[streamName] = streamDesc
		}
		res[k] = tres
	}
	return res
}

func (rtmpd *RtmpCli) Save() error {
	glog.Infoln("RtmpCli.Save")
	rtmpd.RLock()
	defer rtmpd.RUnlock()

	fp, err := ioutil.TempFile("/opt/avs", "rtmp")
	if err != nil {
		return err
	}
	defer fp.Close()
	defer os.Remove(fp.Name())

	err = json.NewEncoder(fp).Encode(rtmpd.filterStreams(rtmpd.states))
	if err != nil {
		return err
	}
	_ = os.Rename("/opt/avs/streams.json", "/opt/avs/streams.json.bak")
	return os.Rename(fp.Name(), "/opt/avs/streams.json")
}

func (rtmpd *RtmpCli) Load() error {
	glog.Infoln("RtmpCli.Load")
	fp, err := os.Open("/opt/avs/streams.json")
	if err != nil {
		return err
	}
	return json.NewDecoder(fp).Decode(&rtmpd.states)
}

func (rtmpd *RtmpCli) Init(app *App) {
	glog.Infoln("RtmpCli.Init")
	rtmpd.app = app
	rtmpd.outAddr = getHostIp()
	if err := rtmpd.Load(); err != nil {
		rtmpd.states = make(map[string]StreamState)
	}
	go rtmpd.PeriodicUpdate()
	go rtmpd.PeriodicSave()
}

func (rtmpd *RtmpCli) Close() error {
	glog.Infoln("RtmpCli.Close")
	return nil
}

func (rtmpd *RtmpCli) reqRep(conn net.Conn, req Req, res interface{}) error {
	glog.Infoln("RtmpCli.reqRep")
	var length int32
	rep := Rep{Data: res}
	if err := req.Send(conn); err != nil {
		return err
	}
	if err := binary.Read(conn, binary.BigEndian, &length); err != nil {
		glog.Errorf("RtmpCli.reqRep: Error reading response from RTMP server: %v", err)
		return err
	}

	fmt.Fprintf(conn, "exit\r\n")
	data, err := ioutil.ReadAll(conn)
	if err != nil {
		glog.Errorf("RtmpCli.reqRep: Error reading response from RTMP server: %v", err)
		return err
	}
	if int(length) != len(data) {
		glog.Warningf("RtmpCli.reqRep: Warning: length of stream data %d while signalled by server %d", len(data), length)
	}

	if err := json.Unmarshal(data, &rep); err != nil {
		glog.Errorf("RtmpCli.reqRep: Error unmarshalling response from RTMP server: %v", err)
		return err
	}

	if rep.Status != "SUCCESS" {
		glog.Errorf("RtmpCli.reqRep: Error parsed from RTMP server: %s", rep.Description)
		return errors.New(rep.Description)
	}
	return nil
}

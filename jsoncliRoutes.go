package main

import (
	"github.com/andviro/noodle/render"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (rtmpd *RtmpCli) List(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("RtmpCli.List")
	rtmpd.RLock()
	defer rtmpd.RUnlock()

	states := rtmpd.states
	if r.URL.Query().Get("all") == "" {
		states = rtmpd.filterStreams(states)
	}
	if tmp := r.URL.Query().Get("ppe"); tmp != "" {
		states = rtmpd.filterStreamsByPPE(states, tmp)
	}

	res := ListResult{Results: make(StreamState)}
	for _, st := range states {
		for name, value := range st {
			res.Results[name] = value
		}
	}
	res.NumResults = len(res.Results)
	res.Page = 1
	res.TotalPages = 1
	res.ResultsPerPage = res.NumResults
	return render.Yield(c, 200, &res)
}

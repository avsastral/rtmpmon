package main

import (
	"flag"
	"github.com/golang/glog"
)

var version string = "0.0.0-dev"

func main() {
	mediaDir := flag.String("mediaDir", "/opt/avs/media", "Media directory")
	logDir := flag.String("logDir", "/opt/avs/log", "Log directory")
	port := flag.String("port", "http://localhost:5005", "Socket address")
	configFile := flag.String("config", "/opt/avs/ppe.ini", "Config file")
	stateDbPath := flag.String("stateDb", "/opt/avs/state.bdb", "State DB file")
	dbPath := flag.String("dbPath", "/opt/avs/avs_school.sqlite?cache=shared&mode=rwc&loc=auto", "DB file")
	flag.Set("logtostderr", "1")
	flag.Set("v", "4")
	flag.Parse()
	glog.CopyStandardLogTo("INFO")
	glog.V(INFO).Infof("Astral Video Stream Backend v%s\n", version)

	app := AppFactory(*port, *mediaDir, *logDir, *configFile, *stateDbPath, *dbPath)
	defer app.Close()
	glog.Fatal(app.Run())
}

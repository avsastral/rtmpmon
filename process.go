package main

import (
	"fmt"
	"github.com/golang/glog"
	"io"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"time"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"gopkg.in/natefinch/lumberjack.v2"
)

//var videoResolutionRe = regexp.MustCompile(`[0-9]+x[0-9]+`)

type State func(context.Context) (context.Context, State)

const cmdStartTimeout = time.Second * 30
const FFmpegStreamCheckPeriod = time.Second * 5

type Process struct {
	Uid                 string   `json:"uid"`
	Cmd                 string   `json:"cmd"`
	Args                []string `json:"args"`
	Dir                 string   `json:"dir"`
	LogDir              string   `json:"-"`
	StartAttempt        int      `json:"startAttempt"`
	StartTimeout        int      `json:"-"`
	StopAttempt         int      `json:"stopAttempt"`
	StopTimeout         int      `json:"-"`
	BackoffTimeout      int      `json:"-"`
	BackoffMaxTimeout   int      `json:"-"`
	MaxStartAttempts    int      `json:"-"`
	State               string   `json:"state"`
	LastError           error    `json:"lastError"`
	StopOnSuccess       bool     `json:"stopOnSuccess"`
	LocalProxyAddr      string   `json:"-"`
	isFfmpeg            bool     // флаг что это FFmpeg процесс
	slave               bool     // признак того, что процесс - ведомый (есть родительский), пример push - ведомый для pull
	stateHooks          []func(*Process) error
	healthCheckHooks    []func(int64) error
	recSegmentDoneHooks []func(string) error // хук окончания записи кусочка
	curSegmentPath      string
	cancel              context.CancelFunc
	cmd                 *exec.Cmd
	healthCheckRes      chan int
	result              chan error
	state               State
	finished            chan struct{}
	logger              io.WriteCloser
	ffmpegLogger        *FfmpegLogParser
	Meta                interface{}
}

func NewProcess(id, cmd string, args ...string) *Process {
	glog.Infoln("Process.NewProcess")
	if id == "" {
		id = uuid.NewV4().String()
	}
	res := Process{
		Uid:               id,
		Cmd:               cmd,
		Args:              args,
		StartTimeout:      10,
		StopTimeout:       2,
		BackoffTimeout:    3,
		BackoffMaxTimeout: 60,
		MaxStartAttempts:  10000,
		State:             "IDLE",
		result:            make(chan error),
		healthCheckRes:    make(chan int),
		finished:          make(chan struct{}),
		isFfmpeg:          cmd == "ffmpeg",
	}
	return &res
}

func (p *Process) CalcBackoffTimeout() int {
	restartTime := p.BackoffTimeout * p.StartAttempt
	if restartTime > p.BackoffMaxTimeout {
		glog.Warningln("Process.CalcBackoffTimeout: max timeout reached -> calc threshold value")
		// максимальный таймаут ограничим сверху и добавим рандомность,
		// ибо в особо "плохих" ситуациях, период старта будет адово-долгим
		// например 660-я попытка будет через час практически
		rand.Seed(time.Now().UnixNano())
		restartTime = p.BackoffMaxTimeout + rand.Intn(p.BackoffMaxTimeout/2)
	}
	return restartTime
}

func (p *Process) OnState(hooks ...func(*Process) error) {
	p.stateHooks = hooks
}

func (p *Process) OnSegmentDone(hooks ...func(string2 string) error) {
	p.recSegmentDoneHooks = hooks
}

func (p *Process) OnHealthCheck(hooks ...func(int642 int64) error) {
	p.healthCheckHooks = hooks
}

func (p *Process) PrintToLog(message string) {
	if p.logger != nil {
		if message == "" {
			message = "\n\n"
		} else {
			message = fmt.Sprintf("[%s]: %s", time.Now().Format("2006-01-02 15:04:05.000000000"), message)
		}
		if _, err := fmt.Fprintln(p.logger, message); err != nil {
			glog.Errorf("Process.PrintToLog: Error writing to process log: %v", err)
		}
	}
}

// XXX Black magic XXX
func (state State) Name() string {
	names := strings.Split(runtime.FuncForPC(reflect.ValueOf(state).Pointer()).Name(), ".")
	res := strings.ToUpper(names[len(names)-1])
	return res[:len(res)-3]
}

func (p *Process) pkill(marker string) error {
	processPattern := fmt.Sprintf("%s.*%s", p.Cmd, marker)
	glog.Infof("Process.pkill: %s(%v) :: %s %v", p.Cmd, p.Uid, "pkill", []string{"-f", processPattern, "--signal", "9"})
	cmd := exec.Command("pkill", []string{"-f", processPattern, "--signal", "9"}...)
	err := cmd.Start()
	if err != nil {
		glog.Errorf("Process.pkill: %s(%v) failed to pkill with error %v", cmd, p.Uid, err)
		return err
	}
	return cmd.Wait()
}

func (p *Process) Transition(s State) {
	p.state = s
	if s == nil {
		return
	}
	p.State = p.state.Name()
	glog.Infof("Process.Transition: %s(%v) to state %s", p.Cmd, p.Uid, p.State)
	for _, h := range p.stateHooks {
		err := h(p)
		if err != nil {
			glog.Infof("Process.Transition: %s(%v) state hook error %v", p.Cmd, p.Uid, err)
			break
		}
	}
}

func (p *Process) Run(parent context.Context) context.Context {
	glog.Infof("Process.Run: %s(%v)", p.Cmd, p.Uid)
	var c context.Context
	if p.state != nil {
		glog.Infof("Process.Run: %s(%v) already running nothing to do here", p.Cmd, p.Uid)
		return c
	}

	if p.LogDir != "" {
		p.logger = &lumberjack.Logger{
			Filename:   filepath.Join(p.LogDir, p.Uid+".log"),
			MaxSize:    10, // megabytes
			MaxBackups: 10,
			MaxAge:     28, //days
		}
	}
	p.finished = make(chan struct{})
	if parent == nil {
		parent = context.Background()
	} else {
		p.slave = true
	}
	c, p.cancel = context.WithCancel(parent)
	p.Transition(p.Starting)
	go func() {
		var next State

		for p.state != nil {
			c, next = p.state(c)
			p.Transition(next)
		}
		close(p.finished)
		// перед выходом обсчитаем последний кусок-сегмент
		if p.curSegmentPath != "" {
			for _, h := range p.recSegmentDoneHooks {
				if err := h(p.curSegmentPath); err != nil {
					glog.Infof("Process.Run: %s(%v) segment done hook error %v", p.Cmd, p.Uid, err)
					break
				}
			}
		}
		p.curSegmentPath = ""

		glog.Infof("Process.Run: %s(%v) finished with error %v", p.Cmd, p.Uid, p.LastError)
	}()
	return c
}

func (p *Process) Starting(c context.Context) (context.Context, State) {
	glog.Infof("Process.Starting: %s(%v)", p.Cmd, p.Uid)
	p.cmd = exec.Command(p.Cmd, p.Args...)
	p.cmd.Dir = p.Dir

	running := make(chan bool, 1)
	if p.isFfmpeg {
		//if p.ffmpegLogger != nil {
		//	p.ffmpegLogger.Cleanup()
		//}
		p.ffmpegLogger = NewFfmpegLogParser(p.logger)
		p.cmd.Stderr = p.ffmpegLogger
		go func() {
			defer close(running)
			select {
			case p.Meta = <-p.ffmpegLogger.Res:
				running <- true
			case <-time.After(cmdStartTimeout):
				running <- false
			}
		}()
	} else {
		p.cmd.Stderr = p.logger
		p.cmd.Stdout = p.logger
		go func() {
			<-time.After(time.Duration(p.StartTimeout) * time.Second)
			running <- true
			close(running)
		}()
	}
	p.PrintToLog("")
	p.PrintToLog(fmt.Sprintf("%s %v\n******* LOG BEGIN *******\n", p.Cmd, p.Args))

	p.LastError = p.cmd.Start()
	if p.LastError != nil {
		glog.Errorf("Process.Starting: %s(%v) failed to start with error %v", p.Cmd, p.Uid, p.LastError)
		return c, p.Failed
	}
	go func() {
		p.result <- p.cmd.Wait()
	}()

	select {
	case <-c.Done():
		glog.Infof("Process.Starting: %s(%v) received cancel signal", p.Cmd, p.Uid)
		return c, p.Stopping
	case p.LastError = <-p.result:
		glog.Errorf("Process.Starting: %s(%v) finished with error %v", p.Cmd, p.Uid, p.LastError)
		if p.LastError == nil && p.StopOnSuccess {
			return c, p.Stopped
		}
		// если была ошибка - проверим если это ffmpeg м.б. фатальная ошибка AlreadyPublished
		if p.isFfmpeg && !p.ffmpegCheckForAlreadyPublishedError() {
			return c, p.KillingByPattern
		}
		// если фаталити не обнаружилось - просто пытаемся запуститься заново
		p.StartAttempt++
		return c, p.Backoff
	case startRes := <-running:
		if !startRes {
			p.PrintToLog("==> AVS: Failed to start ffmpeg, timeout reached, restarting\n\n")
			glog.Warningf("Process.Starting: %s(%v) timeout reached - restarting\n", p.Cmd, p.Uid)
			return c, p.Killing
		}

		p.PrintToLog("==> AVS: ffmpeg started successfully")
		glog.Infof("Process.Starting: %s(%v) started successfully\n", p.Cmd, p.Uid)
		p.LastError = nil
		p.StartAttempt = 0
	}
	return c, p.Running
}

func (p *Process) Running(c context.Context) (context.Context, State) {
	glog.Infof("Process.Running: %s(%v)", p.Cmd, p.Uid)
	stopChecking := make(chan struct{}, 1)
	stopSegmentWatching := make(chan struct{}, 1)
	defer close(stopChecking)
	defer close(stopSegmentWatching)
	if p.isFfmpeg {
		// для ffmpeg
		// - самокортроль потоков
		go p.ffmpegCheckForStreamErrors(stopChecking)
		// - мониторинг файлов (сегментов) записи
		go p.ffmpegRecSegmentWatching(stopSegmentWatching)
	}

	select {
	case healthCheckRes := <-p.healthCheckRes:
		if healthCheckRes == FFmpegProcessKillByPattern {
			glog.Infof("Process.Running: %s(%v) request cancel", p.Cmd, p.Uid)
			return c, p.KillingByPattern
		} else {
			glog.Infof("Process.Running: %s(%v) request reset", p.Cmd, p.Uid)
			return c, p.Killing
		}
	case <-c.Done():
		glog.Infof("Process.Running: %s(%v) received cancel signal", p.Cmd, p.Uid)
		return c, p.Stopping
	case p.LastError = <-p.result:
		glog.Infof("Process.Running: %s(%v) exit with error %v", p.Cmd, p.Uid, p.LastError)
		if p.LastError == nil && p.StopOnSuccess {
			return c, p.Stopped
		}
		// если была ошибка - проверим если это ffmpeg м.б. фатальная ошибка AlreadyPublished
		if p.isFfmpeg && !p.ffmpegCheckForAlreadyPublishedError() {
			return c, p.KillingByPattern
		}
		// если фаталити не обнаружилось - просто пытаемся запуститься заново
		return c, p.Restarting
	}
}

func (p *Process) Stopping(c context.Context) (context.Context, State) {
	glog.Infof("Process.Stopping: %s(%v)", p.Cmd, p.Uid)
	if p.LastError = p.cmd.Process.Signal(os.Interrupt); p.LastError != nil {
		glog.Errorf("Process.Stopping: %s(%v) failed to stop with error: %s", p.Cmd, p.Uid, p.LastError)
		return c, p.Failed
	}

	select {
	case p.LastError = <-p.result:
		glog.Infof("Process.Stopping: %s(%v) stopped successfully", p.Cmd, p.Uid)
		p.StopAttempt = 0
		break
	case <-time.After(time.Duration(p.StopTimeout) * time.Second):
		glog.Warningf("Process.Stopping: %s(%v) reached timeout -> try one more time", p.Cmd, p.Uid)
		p.LastError = nil
		p.StopAttempt++
		return c, p.Stopping
	}
	return c, p.Stopped
}

func (p *Process) Killing(c context.Context) (context.Context, State) {
	glog.Infof("Process.Killing: %s(%v)", p.Cmd, p.Uid)
	if p.LastError = p.cmd.Process.Signal(os.Kill); p.LastError != nil {
		glog.Errorf("Process.Killing: %s(%v) failed to kill with error: %s", p.Cmd, p.Uid, p.LastError)
		return c, p.Restarting
	}

	select {
	case p.LastError = <-p.result:
		glog.Infof("Process.Killing: %s(%v) killed successfully", p.Cmd, p.Uid)
		break
	case <-time.After(time.Duration(p.StopTimeout) * time.Second):
		glog.Warningf("Process.Killing: %s(%v) reached timeout -> try one more time", p.Cmd, p.Uid)
		p.LastError = nil
		return c, p.Killing
	}
	return c, p.Restarting
}

func (p *Process) KillingByPattern(c context.Context) (context.Context, State) {
	glog.Infof("Process.KillingByPattern: %s(%v)", p.Cmd, p.Uid)
	killRes := make(chan error)
	defer close(killRes)
	go func() {
		killRes <- p.pkill(p.LocalProxyAddr)
	}()
	select {
	case p.LastError = <-killRes:
		glog.Infof("Process.KillingByPattern: %s(%v) killed with errors: %v", p.Cmd, p.Uid, p.LastError)
		break
	case <-time.After(time.Duration(p.StopTimeout) * time.Second):
		glog.Warningf("Process.KillingByPattern: %s(%v) reached timeout -> try one more time", p.Cmd, p.Uid)
		p.LastError = nil
		return c, p.KillingByPattern
	}
	return c, p.Restarting
}

func (p *Process) Backoff(c context.Context) (context.Context, State) {
	glog.Infof("Process.Backoff: %s(%v)", p.Cmd, p.Uid)
	if p.StartAttempt > p.MaxStartAttempts {
		glog.Errorf("Process.Backoff: %s(%v) backoff failed: max attempts reached -> failed", p.Cmd, p.Uid)
		return c, p.Failed
	}
	select {
	case <-c.Done():
		glog.Infof("Process.Backoff: %s(%v) received cancel signal", p.Cmd, p.Uid)
		return c, p.Stopping
	case <-time.After(time.Duration(p.CalcBackoffTimeout()) * time.Second):
		glog.Infof("Process.Backoff: %s(%v) wait for backoff timeout complete -> starting", p.Cmd, p.Uid)
		p.LastError = nil
	}
	return c, p.Starting
}

func (p *Process) Failed(c context.Context) (context.Context, State) {
	glog.Infof("Process.Failed: %s(%v)", p.Cmd, p.Uid)
	return c, nil
}

func (p *Process) Restarting(c context.Context) (context.Context, State) {
	glog.Infof("Process.Restarting: %s(%v)", p.Cmd, p.Uid)
	return c, p.Starting
}

func (p *Process) Stopped(c context.Context) (context.Context, State) {
	glog.Infof("Process.Stopped: %s(%v)", p.Cmd, p.Uid)
	return c, nil
}

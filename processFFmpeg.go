package main

import (
	"github.com/golang/glog"
	"time"
)

const (
	FFmpegProcessKill          = 0
	FFmpegProcessKillByPattern = 1
)

func (p *Process) ffmpegCheckForAlreadyPublishedError() bool {
	glog.Infof("Process.ffmpegCheckForAlreadyPublishedError: %s(%v)", p.Cmd, p.Uid)
	if p.ffmpegLogger.AlreadyPublished {
		p.PrintToLog("==> AVS: Stream check head => !!!ALARM!!! ALREADY PUBLISHING, stopping")
		return false
	}
	p.PrintToLog("==> AVS: Stream check head => OK")
	return true
}

func (p *Process) ffmpegRecSegmentWatching(stopChecking chan struct{}) {
	glog.Infof("Process.ffmpegRecSegmentWatching: %s(%v) activated", p.Cmd, p.Uid)
	segment := ""
	for {
		select {
		case segment = <-p.ffmpegLogger.RecSegmentChan:
			glog.Infof("record got new segment file: %s", segment)
			if p.curSegmentPath == "" {
				p.curSegmentPath = segment
			} else {
				for _, h := range p.recSegmentDoneHooks {
					if err := h(p.curSegmentPath); err != nil {
						glog.Infof("Process.Run: %s(%v) segment done hook error %v", p.Cmd, p.Uid, err)
						break
					}
				}
				p.curSegmentPath = segment
			}
		case <-stopChecking:
			return
		}
	}
}

func (p *Process) ffmpegCheckForStreamErrors(stopChecking chan struct{}) {
	glog.Infof("Process.ffmpegCheckForStreamErrors: %s(%v) healthCheck activated", p.Cmd, p.Uid)
	p.PrintToLog("==> AVS: Stream health check => ACTIVATED")
	defer p.PrintToLog("==> AVS: Stream health check => DEACTIVATED (Process exit from running state)")
	defer glog.Infof("Process.ffmpegCheckForStreamErrors: %s(%v) exit from running state -> healthCheck deactivated", p.Cmd, p.Uid)

	// первым делом проверим на предмет фаталити с AlreadyPublished
	// todo: здесь кажется не надо проверитья (но это не точно) данная ошибка валится на starting
	if !p.ffmpegCheckForAlreadyPublishedError() {
		p.healthCheckRes <- FFmpegProcessKillByPattern
		return
	}

	for {
		select {
		case <-stopChecking:
			return
		case <-time.After(FFmpegStreamCheckPeriod):
			// пришло время проверить чо там c нашим потоком
			if !p.ffmpegAnalyzeStreamHealthCheck(p.ffmpegLogger.HealthCheck()) {
				p.healthCheckRes <- FFmpegProcessKill
				return
			}
		}
	}
}

func (p *Process) ffmpegAnalyzeStreamHealthCheck(healthCheckRes uint64) bool {
	switch healthCheckRes {
	case FFmpegNoErrors:
		p.PrintToLog("==> AVS: Stream health check => OK")
		return true
	case FFmpegPossibleNoVideo:
		p.PrintToLog("==> AVS: Stream health check => NO FRAMES (no video frames received from last checkpoint), restarting")
		return false
	case FFmpegStuck:
		p.PrintToLog("==> AVS: Stream health check => STUCK (no data received from last checkpoint), restarting")
		return false
	case FFmpegNonMonotonousDTS:
		p.PrintToLog("==> AVS: Stream health check => NON-MONOTONOUS DTS, restarting")
		return false
	case FFmpegSTInvalidDropping:
		p.PrintToLog("==> AVS: Stream health check => ST-INVALID DROPPING, restarting")
		return false
	default:
		p.PrintToLog("==> AVS: Stream health check => UNKNOWN RESULT, continue")
		return true
	}
}

package main

import (
	"fmt"
	"github.com/golang/glog"
	"strings"
	"sync"
)

type Pulls struct {
	streams map[string]StreamDesc
	Locker
	*App
}

type StreamDesc struct {
	Uri        string `json:"uri" form:"uri" binding:"required"`
	StreamName string `json:"streamName" form:"streamName" binding:"required" storm:"id"`
	ForceTcp   bool   `json:"forceTcp" form:"forceTcp"`
	OVZ        bool   `json:"OVZ" form:"OVZ"`
	Title      string `json:"title" form:"title"`
	*Process
	relay *Process
}

func (pulls *Pulls) Init(app *App) {
	glog.Infoln("Pulls.Init")
	pulls.App = app
	app.pulls = pulls
	pulls.streams = make(map[string]StreamDesc)
	if err := app.store.Init(&StreamDesc{}); err != nil {
		glog.Errorf("Pulls.Init: Error to initialize store: %s", err)
	}
	app.OnClose(pulls.Close)
}

func (pulls *Pulls) Close() error {
	glog.Infoln("Pulls.Close")
	// pull-потоки не будем завершать - пофигу - они умрут вместе с докером
	// Надо завершить все процессы, удалять из внутреннего хранишища смысла нет, поскольку это ти так выход
	//pulls.Locker.mu.RLock()
	//defer pulls.Locker.mu.RUnlock()
	//streamNames := make([]string, 0, len(pulls.streams))
	//for k := range pulls.streams {
	//	streamNames = append(streamNames, k)
	//}
	//pulls.stopProcesses(streamNames)
	return nil
}

func (pulls *Pulls) Restore() {
	glog.Infoln("Pulls.Restore")
	var streams []StreamDesc
	glog.Infoln("Pulls.Restore: Loading pull state")
	if err := pulls.App.store.All(&streams); err != nil {
		glog.Errorf("Pulls.Restore: Error loading pull state: %v", err)
		return
	}
	glog.Infoln("Pulls.Restore: Loading cameras list")
	cams := pulls.App.sys.Cameras()
	n := 0
	pulls.Locker.mu.Lock()
	defer pulls.Locker.mu.Unlock()
	for _, stream := range streams {
		glog.Infoln("Pulls.Restore: restoring stream: ", stream.StreamName)
		if _, ok := cams[stream.StreamName]; !ok {
			glog.Warningf("Pulls.Restore: No camera data for pulled stream %s - skipping", stream.StreamName)
			if err := pulls.App.store.Remove(&stream); err != nil {
				glog.Errorf("Pulls.Restore: Error removing pulled stream from state: %v", err)
			}
			continue
		}
		if err := pulls.addOne(stream); err != nil {
			glog.Errorf("Pulls.Restore: Error adding saved stream: %v", err)
			continue
		}
		n++
		glog.Infof("Pulling stream %s from %s", stream.StreamName, stream.Uri)
	}
	glog.Infof("Restored %d pulled streams", n)
}

func (pulls *Pulls) Greet(e EventStream) {
	glog.Infoln("Pulls.Greet")
	pullEventData := make(map[string]StreamDesc)
	pushEventData := make(map[string]PushSse)
	func () {
		pulls.Locker.mu.RLock()
		defer pulls.Locker.mu.RUnlock()
		for k,v := range pulls.streams {
			pullEventData[k] = v
			if v.relay != nil {
				pushEventData[k] = PushSse{
					StreamName: k,
					State:      v.relay.State,
					LastError:  v.relay.LastError,
				}
			}
		}
	}()
	e <- Event{"pull", pullEventData}
	e <- Event{"push", pushEventData}
	glog.Infoln("Pulls.Greet: sent message to sse")
}

func(pulls *Pulls) getDestName(camComment, streamName string) (res string) {
	glog.Infoln("Pulls.getDestName")
	//var destName string
	if pulls.App.cfg.newStreamNames && checkDescRe.MatchString(camComment) {
		res = camComment
	} else {
		res = streamName
	}
	if override, ok := pulls.App.cfg.overrides[res]; ok {
		glog.Infof("Pulls.getDestName: Override stream %s to %s", res, override)
		res = override
	}
	return
}

func (pulls *Pulls) ffmpegArgs(streamUri, nginxUri, streamName string) (res []string) {
	// РД
	//*** WORK											*** DEV (not work in RD)
	//	-hide_banner									-hide_banner
	//	-rtsp_flags prefer_tcp							-rtsp_flags prefer_tcp
	//	-i "rtsp://бла-бла-бла"							-i "rtsp://бла-бла-бла"
	//	-r 15
	//	-copytb 1
	//	-vsync 1										-vsync 1
	//	-async 1										-async 1
	//	-vcodec h264									-vcodec copy
	//	-preset ultrafast
	//	-crf 0
	//	-acodec aac										-acodec aac
	//	-metadata title=050021_1						-metadata title=050021_1
	//	-f flv rtmp://localhost:1935/vod/050021_1		-f flv rtmp://localhost:1935/vod/050021_1
	glog.Infoln("Pulls.ffmpegArgs")
	res = []string {
		"-hide_banner",
	}
	if strings.HasPrefix(streamUri, "rtsp") && pulls.App.cfg.preferTCP {
		res = append(res, "-rtsp_flags", "prefer_tcp")
	}
	res = append(res, "-i", streamUri)

	if GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-r", "") != "" {
		res = append(res, "-r", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-r", ""))
	}
	if GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-copytb", "") != "" {
		res = append(res, "-copytb", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-copytb", ""))
	}

	res = append(res,
		"-vsync", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-vsync", "1"),
		"-async", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-async", "1"),
	)
	defVcodec := "copy"
	defPreset := ""
	if strings.HasPrefix(streamUri, "/dev") {
		defVcodec = "libx264"
		defPreset = "ultrafast"
	}
	res = append(res, "-vcodec", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-vcodec", defVcodec))

	if GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-preset", defPreset) != "" {
		res = append(res, "-preset", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-preset", defPreset))
	}
	if GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-crf", "") != "" {
		res = append(res, "-crf", GetFfmpegArg(pulls.App.cfg.ffmpegPullOverrideAgrs, "-crf", ""))
	}
	res = append(res,
		//"-acodec", "pcm_mulaw",
		"-acodec", "aac",
		//"-an",
		"-metadata", "title="+streamName,
		"-f", "flv",
		nginxUri,
	)

	return
}

func (pulls *Pulls) addOne(s StreamDesc) error {
	glog.Infoln("Pulls.addOne")
	var camInfo CamDesc
	var err error
	t := strings.Split(s.StreamName, "_")
	if camInfo, err = pulls.App.sys.Camera(t[1]); err != nil {
		glog.Errorf("Pulls.addOne: Error getting Camera info from DB: %v", err)
		return err
	}
	if st, ok := pulls.streams[s.StreamName]; ok {
		if st.OVZ == s.OVZ && st.Process.state != nil {
			glog.Infof("Pulls.addOne: Stream %s not changed", s.StreamName)
			return nil
		} else {
			st.Process.cancel()
			<-st.Process.finished
			delete(pulls.streams, s.StreamName)
		}
	}
	destUri := fmt.Sprintf("rtmp://localhost:1935/vod/%s", s.StreamName)
	glog.Infof("Pulls.addOne: Pulling stream %s from %s to %s", s.StreamName, s.Uri, destUri)

	args := pulls.ffmpegArgs(s.Uri, destUri, s.StreamName)
	s.Process = NewProcess("pull_"+s.StreamName, "ffmpeg", args...)
	s.Process.LocalProxyAddr = destUri
	s.Process.LogDir = pulls.App.logDir
	s.Process.OnState(func(pr *Process) error {
		pulls.App.bus.Events <- Event{"pull", map[string]StreamDesc{s.StreamName: s}}
		return nil
	})
	ctx := s.Process.Run(nil)
	if !s.OVZ {
		destName := pulls.getDestName(camInfo.Comment, s.StreamName)
		targetURI := pulls.App.relay.Route(destName)
		s.relay = pulls.App.pushes.createProcess(
			Push{
				LocalName:  s.StreamName,
				TargetURI:  targetURI,
				TargetName: destName,
				Title:      s.Title,
				OVZ:        s.OVZ,
			},
		)
		s.relay.OnState(func(pr *Process) error {
			pulls.App.bus.Events <- Event{"push", map[string]PushSse{s.StreamName: {
				StreamName: s.StreamName,
				State:      pr.State,
				LastError:  pr.LastError,
			}}}
			return nil
		})
		s.relay.Run(ctx)
	}
	pulls.streams[s.StreamName] = s
	return nil
}

func (pulls *Pulls) delOne(streamName string) error {
	glog.Infoln("Pulls.delOne")
	if st, ok := pulls.streams[streamName]; ok {
		st.cancel()
		<-st.finished
		delete(pulls.streams, streamName)
		if err := pulls.App.store.Remove(StreamDesc{StreamName: streamName}); err != nil {
			glog.Errorf("Pulls.delOne: Error deleting pulled stream %s from store: %v", streamName, err)
			return err
		}
		glog.Infoln("Pulls.delOne: Stopped pulling stream %s from %s", streamName, st.Uri)
	}
	return nil
}

func (pulls *Pulls) stopProcesses(streamNames []string) {
	glog.Infoln("Pulls.stopProcesses:", streamNames)
	var wg sync.WaitGroup
	for _, streamName := range streamNames {
		wg.Add(1)
		go func(sn string) {
			defer wg.Done()
			if st, ok := pulls.streams[sn]; ok {
				st.cancel()
				<-st.finished
				glog.Infof("Pulls.stopProcesses: Stopped pulling stream %s from %s", sn, st.Uri)
			}
		}(streamName)
	}
	wg.Wait()
}

func (pulls *Pulls) delMany (streamNames []string) (err error) {
	glog.Infoln("Pulls.delMany:", streamNames)
	// остановим процессы
	pulls.stopProcesses(streamNames)
	// удалим из нашего хранишища потоков
	for _, streamName := range streamNames {
		delete(pulls.streams, streamName)
		if removeErr := pulls.App.store.Remove(StreamDesc{StreamName: streamName}); removeErr != nil {
			glog.Errorf("Pulls.delMany: Error deleting pulled stream %s from store: %v", streamName, removeErr)
			err = removeErr
		}
	}
	return
}

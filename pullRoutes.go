package main

import (
	"github.com/andviro/noodle/bind"
	"github.com/andviro/noodle/render"
	"github.com/andviro/noodle/wok"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (pulls *Pulls) Add(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pulls.Route.Add")

	stream := bind.GetData(c).(*StreamDesc)
	if err := pulls.addOne(*stream); err != nil {
		w.WriteHeader(400)
		return err
	}
	if err := pulls.App.store.Save(stream); err != nil {
		w.WriteHeader(500)
		return err
	}
	return render.Yield(c, 201, stream)
}

func (pulls *Pulls) List(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pulls.Route.List")
	return render.Yield(c, 200, pulls.streams)
}

func (pulls *Pulls) AddMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pulls.Route.AddMany")
	streams := bind.GetData(c).(*[]StreamDesc)
	for _, stream := range *streams {
		if err := pulls.addOne(stream); err != nil {
			w.WriteHeader(400)
			return err
		}
		if err := pulls.App.store.Save(stream); err != nil {
			w.WriteHeader(500)
			return err
		}
		glog.Infoln("Pulls.Route.AddMany: Pulling stream %s from %s", stream.StreamName, stream.Uri)
	}
	return render.Yield(c, 201, streams)
}

func (pulls *Pulls) Delete(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pulls.Route.Delete")
	streamName := wok.Var(c, "streamName")
	if err := pulls.delOne(streamName); err != nil {
		w.WriteHeader(400)
		return err
	}
	if err := pulls.App.store.Remove(StreamDesc{StreamName: streamName}); err != nil {
		glog.Errorf("Pulls.Route.Delete: Error deleting pulled stream %s from store: %v", streamName, err)
	}
	return render.Yield(c, 202, M{"streamName": streamName})
}

func (pulls *Pulls) DeleteMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pulls.Route.DeleteMany")
	streamNames := bind.GetData(c).(*[]string)
	if err := pulls.delMany(*streamNames); err != nil {
		w.WriteHeader(400)
		return err
	}
	return render.Yield(c, 202, M{"streamNames": streamNames})
}

//// todo: кажется не используется
//func (p *Pulls) Restart(c context.Context, w http.ResponseWriter, r *http.Request) error {
//	glog.Infoln("Pulls.Route.Restart")
//	name := r.FormValue("name")
//	if st, ok := p.streams[name]; ok {
//		st.Process.cmd.Process.Signal(os.Interrupt)
//	}
//	return render.Yield(c, 202, M{"name": name})
//}

package main

import (
	"github.com/golang/glog"
	"strings"
	"sync"
)

type Pushes struct {
	streams map[string]Push
	Locker
	*App
}

type PushSse struct {
	StreamName string `json:"streamName"`
	State      string `json:"state"`
	LastError  error  `json:"lastError"`
}

type Push struct {
	LocalName  string `json:"localStreamName" form:"localStreamName" binding:"required"`
	TargetName string `json:"targetStreamName" form:"targetStreamName" binding:"required"`
	TargetURI  string `json:"targetURI" form:"targetURI" binding:"required"`
	OVZ        bool   `json:"OVZ" form:"OVZ"`
	Title      string `json:"title" form:"title"`
	*Process   `json:"process"`
}

func (pushes *Pushes) Init(app *App) {
	glog.Infoln("Pushes.Init")
	pushes.App = app
	app.pushes = pushes
	pushes.streams = make(map[string]Push)
	// todo: надо ли нам тут подчищать хвосты?
}

func (pushes *Pushes) ffmpegArgs(LocalName, TargetName, outputFmt, TargetURI string) (res []string) {
	glog.Infoln("Pushes.ffmpegArgs")
	res = []string{
		"-hide_banner",
		//"-use_wallclock_as_timestamps", "1",
		"-i", "rtmp://localhost:1935/vod/" + LocalName,
		"-vsync", GetFfmpegArg(pushes.App.cfg.ffmpegPullOverrideAgrs, "-vsync", "1"),
		"-async", GetFfmpegArg(pushes.App.cfg.ffmpegPullOverrideAgrs, "-async", "1"),
		"-c", GetFfmpegArg(pushes.App.cfg.ffmpegPullOverrideAgrs, "-c", "copy"),
		//"-vcodec", "copy",
		//"-acodec", "copy",
		//"-an",
		"-metadata", "title=" + TargetName,
		"-f", outputFmt,
	}
	if pushes.App.cfg.preferPushTCP && outputFmt == "rtsp" {
		res = append(res,
			"-rtsp_flags", "prefer_tcp",
		)
	}
	if pushes.App.cfg.pushBufSize != "0" {
		res = append(res,
			"-bufsize", pushes.App.cfg.pushBufSize,
		)
	}
	res = append(res,
		UrlJoin(TargetURI, TargetName),
	)
	return
}

func (pushes *Pushes) createProcess(stream Push) *Process {
	glog.Infof("Pushes.createProcess: Pushing stream %s to %s as %s", stream.LocalName, stream.TargetURI, stream.TargetName)

	var outputFmt = "rtsp"
	if strings.HasPrefix(stream.TargetURI, "rtmp") {
		outputFmt = "flv"
	}

	args := pushes.ffmpegArgs(stream.LocalName, stream.TargetName, outputFmt, stream.TargetURI)

	res := NewProcess("push_"+stream.LocalName, "ffmpeg", args...)
	res.LogDir = pushes.App.logDir
	return res
}

func (pushes *Pushes) addOne(stream Push) error {
	glog.Infof("Pushes.addOne")
	if _, ok := pushes.streams[stream.LocalName]; ok {
		glog.Infof("Pushes.addOne: Push for %s already exists", stream.LocalName)
		return nil
	}
	stream.Process = pushes.createProcess(stream)
	pushes.streams[stream.LocalName] = stream
	stream.Process.Run(nil)
	return nil
}

func (pushes *Pushes) delMany(streamNames []string) (err error) {
	glog.Infoln("Pushes.delMany:", streamNames)
	// остановим процессы
	pushes.stopProcesses(streamNames)
	// удалим из нашего хранишища потоков
	for _, streamName := range streamNames {
		delete(pushes.streams, streamName)
		if remErr := pushes.App.store.Remove(StreamDesc{StreamName: streamName}); remErr != nil {
			glog.Errorf("Pulls.delMany: Error deleting pulled stream %s from store: %v", streamName, remErr)
			err = remErr
		}
	}
	return
}

func (pushes *Pushes) delOne(streamName string) error {
	glog.Infoln("Pushes.delOne:", streamName)
	if st, ok := pushes.streams[streamName]; ok {
		st.cancel()
		<-st.finished
		delete(pushes.streams, streamName)
		glog.Infoln("Pushes.delOne: Stopped pushing stream %s", streamName)
		if remErr := pushes.App.store.Remove(StreamDesc{StreamName: streamName}); remErr != nil {
			glog.Errorf("Pulls.delOne: Error deleting pulled stream %s from store: %v", streamName, remErr)
			return remErr
		}
	}
	return nil
}

func (pushes *Pushes) stopProcesses(streamNames []string) {
	glog.Infoln("Pushes.stopProcesses:", streamNames)
	var wg sync.WaitGroup
	for _, streamName := range streamNames {
		wg.Add(1)
		go func(sn string) {
			defer wg.Done()
			if st, ok := pushes.streams[sn]; ok {
				st.cancel()
				<-st.finished
				glog.Infof("Pulls.stopProcesses: Stopped pulling stream %s to %s", sn, st.TargetURI)
			}
		}(streamName)
	}
	wg.Wait()
}

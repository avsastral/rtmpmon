package main

import (
	"github.com/andviro/noodle/bind"
	"github.com/andviro/noodle/render"
	"github.com/andviro/noodle/wok"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (pushes *Pushes) Add(c context.Context, w http.ResponseWriter, r *http.Request) error {
	stream := bind.GetData(c).(*Push)
	if err := pushes.addOne(*stream); err != nil {
		w.WriteHeader(400)
		return err
	}
	return render.Yield(c, 201, stream)
}

func (pushes *Pushes) AddMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pushes.Route.AddMany")
	streams := bind.GetData(c).(*[]Push)
	for _, stream := range *streams {
		if err := pushes.addOne(stream); err != nil {
			w.WriteHeader(400)
			return err
		}
	}
	return render.Yield(c, 201, streams)
}

func (pushes *Pushes) List(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pushes.Route.List")
	return render.Yield(c, 200, pushes.streams)
}

func (pushes *Pushes) Delete(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pushes.Route.Delete")
	streamName := wok.Var(c, "streamName")
	if err := pushes.delOne(streamName); err != nil {
		w.WriteHeader(400)
		return err
	}
	return render.Yield(c, 202, M{"localStreamName": streamName})
}

func (pushes *Pushes) DeleteMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Pushes.Route.DeleteMany")
	streamNames := bind.GetData(c).(*[]string)
	if err := pushes.delMany(*streamNames); err != nil {
		w.WriteHeader(400)
		return err
	}
	return render.Yield(c, 202, M{"localStreamNames": streamNames})
}

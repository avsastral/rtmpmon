package main

import (
	"crypto/sha256"
	"fmt"
	"github.com/golang/glog"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

type Records struct {
	records map[string]Record
	*App
	Locker
}

// Mon Jan 2 15:04:05 -0700 MST 2006
const (
	//RecDateFmt = "02-01-2006"
	RecDateFmt = "2006-01-02"
	RecTimeFmt = "15-04-05"
)

type Record struct {
	Name       string `json:"streamName" form:"streamName" binding:"required" storm:"id"`
	SegmentSec int    `json:"segmentSec" form:"segmentSec"`
	LimitSec   int    `json:"limitSec" form:"limitSec"`
	UriRec     string `json:"uriRec" form:"uriRec"`
	OVZ        bool   `json:"OVZ" form:"OVZ"`
	Title      string `json:"title" form:"title"`
	*Process   `json:"process"`
}

func (records *Records) Init(app *App) {
	glog.Infoln("Records.Init")
	records.App = app
	app.records = records

	if err := os.MkdirAll(records.App.mediaDir, 0777); err != nil {
		glog.Fatal("Records.Init: error creating media dir:", err)
	}
	records.records = make(map[string]Record)
	if err := app.store.Init(&Record{}); err != nil {
		glog.Errorf("Records.Init: Error to initialize store: %s", err)
	}
	app.OnClose(records.Close)
}

func (records *Records) Close() error {
	glog.Infoln("Records.Close")
	// Надо завершить все процессы, удалять из внутреннего хранишища смысла нет, поскольку это ти так выход
	// todo: в общем случае этот код не сможет взять блокировку, так как в общем случае - блокировка взята кодом, который никак не завершается и rest бэка висит из-за этого
	records.Locker.mu.RLock()
	defer records.Locker.mu.RUnlock()
	streamNames := make([]string, 0, len(records.records))
	for k := range records.records {
		streamNames = append(streamNames, k)
	}
	records.stopProcesses(streamNames)
	return nil
}

func (records *Records) Restore() {
	glog.Infoln("Records.Restore")
	var recs []Record
	err := records.App.store.All(&recs)
	if err != nil {
		glog.Errorf("Records.Restore: Error loading record state: %v", err)
		return
	}
	cams := records.App.sys.Cameras()
	n := 0
	records.Locker.mu.Lock()
	defer records.Locker.mu.Unlock()
	for _, rec := range recs {
		glog.Infoln("Records.Restore: restoring stream: ", rec.Name)
		if _, ok := cams[rec.Name]; !ok {
			glog.Warningf("Records.Restore: No camera data for recorded stream %s - skipping", rec.Name)
			if err := records.App.store.Remove(&rec); err != nil {
				glog.Errorf("Records.Restore: Error removing recorded stream from state: %v", err)
			}
			continue
		}
		err := records.addOne(rec)
		if err != nil {
			glog.Errorf("Records.Restore: Error adding saved stream: %v", err)
			continue
		}
		n++
		glog.Infof("Recording stream %s from %s", rec.Name, rec.UriRec)
	}
	glog.Infof("Restored %d recorded streams", n)
}

func (records *Records) Greet(e EventStream) {
	glog.Infoln("Records.Greet")
	eventData := make(map[string]Record)
	func() {
		records.Locker.mu.RLock()
		defer records.Locker.mu.RUnlock()
		for k, v := range records.records {
			eventData[k] = v
		}
	}()

	e <- Event{"rec", eventData}
}

func (records *Records) getName(rec Record) (res string) {
	res = rec.Name
	//fields := strings.Split(rec.Name, "_")
	//safeTitle := tlit.MarshalStringURLru(strings.TrimSpace(rec.Title))
	//if len(fields) == 2 && safeTitle != "" {
	//	res = fields[0] + "_" + safeTitle
	//}
	return
}

func (records *Records) getAudience(recName string) (res string, err error) {
	glog.Infoln("Records.getAudience")
	t := strings.Split(recName, "_")
	var camInfo CamDesc
	if camInfo, err = records.App.sys.Camera(t[1]); err != nil {
		glog.Errorf("Records.getAudience: Error getting Camera info from DB: %v", err)
		return "", err
	}
	res = camInfo.Comment
	if res == "" {
		res = recName
	} else {
		res = fmt.Sprintf("%s (%s)", recName, res)
	}
	return
}

func (records *Records) ffmpegArgs(rec Record, recName string) (res []string) {
	uri := fmt.Sprintf("rtmp://localhost:1935/vod/%s", rec.Name)
	if rec.UriRec != "" {
		uri = rec.UriRec
	}

	res = []string{
		"-hide_banner",
	}

	if strings.HasPrefix(uri, "rtsp") && records.App.cfg.preferTCP {
		res = append(res,
			"-rtsp_flags", "prefer_tcp",
		)
	}

	res = append(res,
		"-i", uri,
		//"-copytb", "1",
		"-vsync", "1",
		"-async", "1",
		"-vcodec", "copy",
		"-acodec", "aac",
		"-flags", "global_header",
		//"-flags:v", "+global_header",
		//"-bsf:v", "dump_extra",
		// временно отрубаем игнор ошибок
		//"-err_detect", "ignore_err",
		//"-map", "0",
	)

	if rec.LimitSec != 0 {
		res = append(res, "-t", fmt.Sprintf("%d", rec.LimitSec))
	}

	if records.App.cfg.recordSegmentSecs != 0 {
		glog.Infof("Records.ffmpegArgs: Overriding segmentSec to %d", records.App.cfg.recordSegmentSecs)
		rec.SegmentSec = records.App.cfg.recordSegmentSecs
	}

	if rec.SegmentSec != 0 {
		res = append(res,
			"-f", "segment",
			"-segment_time", fmt.Sprintf("%d", rec.SegmentSec),
		)
		if records.App.cfg.recordWallClock {
			res = append(res,
				"-segment_atclocktime", "1",
			)
		}
		if records.App.cfg.recordResetTimestamps {
			res = append(res,
				"-reset_timestamps", "1",
			)
		}
		// mp4 - очень хрупкий и капризный контейнер - нужна магия - чтобы файл сохранил консистентность,
		// несмотря на "грубое" завершение - отсюда и добавочка segment_format_options
		// примечание: формат mpegts - тоже пашет - но браузер такие файлы отказывается показывать
		res = append(res,
			"-use_wallclock_as_timestamps", "1",
			//"-segment_format", "mpegts",
			"-segment_format", "mp4",
			"-segment_format_options", "movflags=+empty_moov+default_base_moof+omit_tfhd_offset",
			"-strftime", "1",
			recName+"_%03d.mp4",
		)
	} else {
		res = append(res, recName+".mp4")
	}
	return
}

func (records *Records) addOne(rec Record) error {
	glog.Infoln("Records.addOne")
	// todo: разобраться почему так? для pull логика другая - там если процесс запущен - то и пофигу - а здесь он убивается и инитится зановов
	if r, ok := records.records[rec.Name]; ok {
		glog.Warningf("Records.addOne: stream %s is already recorded! We'll stop it", rec.Name)
		r.Process.cancel()
		<-r.Process.finished
		delete(records.records, rec.Name)
	}

	recName := records.getName(rec)
	recAudience, err := records.getAudience(recName)
	if err != nil {
		glog.Errorf("Records.addOne: Error getting audience name: %s", err)
		return err
	}

	glog.Infoln("Adding recording for stream %s", rec.Name)
	if r, ok := records.records[rec.Name]; ok && r.Process.state != nil {
		glog.Infof("Records.addOne: Recording for stream %s already exist", rec.Name)
		return nil
	}

	args := records.ffmpegArgs(rec, recName)
	rec.Process = NewProcess("rec_"+rec.Name, "ffmpeg", args...)
	barrier := make(chan bool)
	go func() {
		glog.Infoln("Records.addOne: Routine folder create")
		for {
			destTime := time.Now().Add(24 * time.Hour)
			year := fmt.Sprintf("%d", destTime.Year())
			destDir := filepath.Join(records.App.mediaDir, year, destTime.Format(RecDateFmt), recAudience)
			if err := os.MkdirAll(destDir, 0777); err != nil {
				glog.Errorf("Records.addOne: Error creating directory %s: %v", destDir, err)
				return
			}
			<-barrier
			select {
			case <-rec.Process.finished:
				glog.Infoln("Records.addOne: Routine folder create: process finished -> exit from routine")
				return
			case <-time.After(1 * time.Hour):
				glog.Infoln("Records.addOne: Routine folder create: 1h -> do job")
				continue
			}
		}
	}()

	rec.Process.OnSegmentDone(func(segmentPath string) error {
		if segmentPath == "" {
			return nil
		}
		fPath := filepath.Join(records.App.mediaDir, segmentPath)
		f, err := os.Open(fPath)
		if err != nil {
			return err
		}
		defer f.Close()

		fi, err := f.Stat()
		if err != nil {
			return err
		}

		h := sha256.New()
		if _, err := io.Copy(h, f); err != nil {
			return err
		}
		hash := fmt.Sprintf("%x", h.Sum(nil))
		_, err = records.App.db.Exec(`
			INSERT INTO cms_records (f_path, f_size, f_modify, f_hash) 
			VALUES (?, ?, ?, ?)`,
			segmentPath, fi.Size(), fi.ModTime().Unix(), hash)
		return err
	})

	rec.Process.OnState(func(p *Process) error {
		// SSE
		records.App.bus.Events <- Event{"rec", map[string]Record{rec.Name: rec}}
		glog.Infoln("Records.addOne::Process.OnState: message to sse was sent")

		switch p.State {
		case "STARTING":
			glog.Infof("Records.addOne::Process.OnState: STARTING")
			now := time.Now()
			year := fmt.Sprintf("%d", now.Year())
			destDir := filepath.Join(records.App.mediaDir, year, now.Format(RecDateFmt), recAudience)
			glog.Infof("Records.addOne::Process.OnState: destDir: %s", destDir)
			if err := os.MkdirAll(destDir, 0777); err != nil {
				glog.Errorf("Records.addOne::Process.OnState: mkdir: %s failed with error: %s", destDir, err)
				p.state = p.Failed
				return err
			}

			glog.Infof("Records.addOne::Process.OnState: destDir: %s created", destDir)
			p.LogDir = records.App.logDir
			n := len(p.Args) - 1
			if rec.SegmentSec != 0 {
				//p.Dir = filepath.Join(records.App.mediaDir, year, now.Format(RecDateFmt), recAudience)
				//p.Args[n] = "%H-%M-%S.mp4"
				p.Dir = filepath.Join(records.App.mediaDir)
				p.Args[n] = "%Y/%Y-%m-%d/" + recAudience + "/%H-%M-%S.mp4"
			} else {
				p.Dir = destDir
				p.Args[n] = now.Format(RecTimeFmt) + ".mp4"
			}

			glog.Infof("Records.addOne: Recording stream '%s' into '%s'", rec.Name, filepath.Join(records.App.mediaDir, year, now.Format(RecDateFmt), recAudience))
			return nil
		default:
			return nil
		}
	})
	records.records[rec.Name] = rec
	if rec.LimitSec != 0 {
		rec.Process.StopOnSuccess = true
	}
	rec.LogDir = records.App.logDir
	barrier <- true
	close(barrier)
	rec.Process.Run(nil)
	return nil
}

func (records *Records) delOne(streamName string) error {
	glog.Infoln("Records.delOne")
	if rec, ok := records.records[streamName]; ok {
		rec.cancel()
		<-rec.finished
		delete(records.records, streamName)
		if err := records.App.store.Remove(Record{Name: streamName}); err != nil {
			glog.Errorf("Records.delOne: Error deleting recorded stream %s from store: %v", streamName, err)
			return err
		}
		glog.Infoln("Records.delOne: Stopped recording stream %s", rec.Name)
	}
	return nil
}

func (records *Records) stopProcesses(streamNames []string) {
	glog.Infoln("Records.stopProcesses:", streamNames)
	var wg sync.WaitGroup
	for _, streamName := range streamNames {
		wg.Add(1)
		go func(sn string) {
			defer wg.Done()
			if rec, ok := records.records[sn]; ok {
				rec.cancel()
				<-rec.finished
				glog.Infof("Records.stopProcesses: recording stream %s from %s", sn, rec.UriRec)
			}
		}(streamName)
	}
	wg.Wait()
}

func (records *Records) delMany(streamNames []string) (err error) {
	glog.Infoln("Records.delMany:", streamNames)
	// остановим процессы
	records.stopProcesses(streamNames)
	// удалим из нашего хранишища потоков
	for _, streamName := range streamNames {
		delete(records.records, streamName)
		if removeErr := records.App.store.Remove(Record{Name: streamName}); removeErr != nil {
			glog.Errorf("Records.delMany: Error deleting recorded stream %s from store: %v", streamName, removeErr)
			err = removeErr
		}
	}
	return nil
}

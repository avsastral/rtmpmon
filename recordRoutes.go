package main

import (
	"github.com/andviro/noodle/bind"
	"github.com/andviro/noodle/render"
	"github.com/andviro/noodle/wok"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (records *Records) Add(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Records.Route.Add")
	rec := bind.GetData(c).(*Record)
	if err := records.addOne(*rec); err != nil {
		w.WriteHeader(500)
		return err
	}
	if err := records.App.store.Save(rec); err != nil {
		w.WriteHeader(500)
		return err
	}
	return render.Yield(c, 201, rec)
}

func (records *Records) AddMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Records.Route.AddMany")
	recs := bind.GetData(c).(*[]Record)
	for _, rec := range *recs {
		if err := records.addOne(rec); err != nil {
			w.WriteHeader(500)
			return err
		}
		if err := records.App.store.Save(rec); err != nil {
			w.WriteHeader(500)
			return err
		}
	}
	return render.Yield(c, 201, recs)
}

func (records *Records) List(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Records.Route.List")
	return render.Yield(c, 200, records.records)
}

func (records *Records) Delete(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Records.Route.Delete")
	streamName := wok.Var(c, "streamName")
	if err := records.delOne(streamName); err != nil {
		w.WriteHeader(500)
		return err
	}
	if err := records.App.store.Remove(Record{Name: streamName}); err != nil {
		w.WriteHeader(500)
		return err
	}
	return render.Yield(c, 202, M{"localStreamName": streamName})
}

func (records *Records) DeleteMany(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Records.Route.DeleteMany")
	streamNames := bind.GetData(c).(*[]string)
	if err := records.delMany(*streamNames); err != nil {
		w.WriteHeader(400)
		return err
	}
	return render.Yield(c, 202, M{"localStreamNames": streamNames})
}

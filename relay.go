package main

import (
	"fmt"
	"github.com/golang/glog"
	"hash/crc32"
	"regexp"
)

var checkDescRe = regexp.MustCompile("[0-9]+_[0-9]+_[0-9]{4}(_[0-9]+)?")

type Relay struct {
	*App
}

func (rl *Relay) Init(app *App) {
	glog.Infoln("Relay.Init")
	rl.App = app
	app.relay = rl
}


func (rl *Relay) offsetForStream(name string) int {
	//glog.Infoln("Relay.offsetForStream")
	if len(name) > 40 {
		return 3000
	}
	sum := crc32.NewIEEE()
	sum.Write([]byte(name))
	n := sum.Sum32()
	return int(n % uint32(rl.App.cfg.publishCoreNumber))
}

func (rl *Relay) Route(localName string) string {
	//glog.Infoln("Relay.Route")
	var basePort = rl.App.cfg.publishBasePort
	if rl.App.cfg.pushMethod == "rtmp" {
		basePort = rl.App.cfg.publishPlayPort
	}
	return fmt.Sprintf(
		"%s://%s:%d/%s",
		rl.App.cfg.pushMethod,
		rl.App.cfg.publishServer,
		basePort+rl.offsetForStream(localName),
		"vod",
	)
}

func (rl *Relay) StreamName(camID int, camDesc string) string {
	glog.Infoln("Relay.StreamName")
	if !checkDescRe.MatchString(camDesc) {
		return fmt.Sprintf("%s_%d", rl.App.cfg.ppe, camID)
	}
	return camDesc
}

package main

import (
	"fmt"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (rl *Relay) OnPublish(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("Relay.Routes.OnPublish")
	target := fmt.Sprintf("rtmp://%s:1935/vod", rl.App.cfg.publishServer)
	glog.Infoln("Relay.Routes.OnPublish: ", target)
	w.Header().Set("Location", target)
	w.WriteHeader(302)
	return nil
}


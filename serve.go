package main

import (
	"fmt"
	"github.com/golang/glog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func (app *App) Run() error {
	glog.Infoln("App.Run")
	var err error
	server := make(chan error)

	glog.Infof("Starting service on %s", app.port)
	glog.Infof("App settings: %#v", app.cfg)
	go func() {
		defer close(server)
		switch {
		case strings.HasPrefix(app.port, "unix://"):
			sockFile := strings.TrimPrefix(app.port, "unix://")
			syscall.Umask(00) // 02
			os.Remove(sockFile)
			l, err := net.Listen("unix", sockFile)
			if err != nil {
				server <- err
				return
			}
			server <- http.Serve(l, app.r)
		case strings.HasPrefix(app.port, "http://"):
			server <- http.ListenAndServe(strings.TrimPrefix(app.port, "http://"), app.r)
		}
	}()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGKILL)
	defer signal.Stop(interrupt)
	defer app.Close()
	select {
	case sig := <-interrupt:
		return fmt.Errorf("Signal received: %v\n", sig)
	case err = <-server:
		return fmt.Errorf("Server exit with error %v\n", err)
	}
}

func (app *App) Close() {
	glog.Infoln("App.Close")
	for _, f := range app.closeHandlers {
		if err := f(); err != nil {
			glog.Errorf("application.Application.Close:: CloseHandler Error: %v", err)
		}
	}
}
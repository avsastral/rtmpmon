package main

import (
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"time"
)

type ServiceWorker struct {
	app  *App
	cancel func()
	contractStartNumCurYear int
	calendarStreams StreamList
	Locker
}

const updateCamsInfoPeriod = 59 * time.Second
const updateCalendarInfoPeriod = 120 * time.Second

func (worker *ServiceWorker) Close() error {
	glog.Infoln("ServiceWorker.Close")
	// а пока просто остановим наши го-рутины
	worker.cancel()
	return nil
}

func (worker *ServiceWorker) Init(app *App) {
	glog.Infoln("ServiceWorker.Init")
	worker.app = app
	ctx, cancel := context.WithCancel(context.Background())
	worker.cancel = cancel
	worker.updateCalendarInfo()
	go worker.periodicUpdateCalendarInfo(ctx)
	go worker.periodicUpdateCamsInfo(ctx)
	app.OnClose(worker.Close)
}

func (worker *ServiceWorker) periodicUpdateCamsInfo(c context.Context) {
	<-worker.app.ready

	glog.Infoln("ServiceWorker.periodicUpdateCamsInfo")
	for {
		// do some stuff
		worker.performUpdateCamsInfo()
		select {
		case <-time.After(updateCamsInfoPeriod):
		case <-c.Done():
			glog.Infoln("ServiceWorker.periodicUpdateCamsInfo :: stop on app close")
			return
		}
	}
}

func (worker *ServiceWorker) periodicUpdateCalendarInfo(c context.Context) {
	<-worker.app.ready

	glog.Infoln("ServiceWorker.periodicUpdateCalendarInfo")
	for {
		// do some stuff
		worker.updateCalendarInfo()
		select {
		case <-time.After(updateCalendarInfoPeriod):
		case <-c.Done():
			glog.Infoln("ServiceWorker.periodicUpdateCalendarInfo :: stop on app close")
			return
		}
	}
}

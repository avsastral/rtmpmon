package main

import "github.com/golang/glog"

func (worker *ServiceWorker) updateCalendarInfo() {
	glog.Infoln("ServiceWorker.updateCalendarInfo")
	var err error
	var streams StreamList
	if streams, err = getCalendarData(worker.app.cfg.avsMonitoringServer, worker.app.cfg.ppe); err != nil {
		glog.Errorln("ServiceWorker.performCheckForAutoStartCams :: ошибка обработки календаря!")
		return
	}
	worker.mu.Lock()
	defer worker.mu.Unlock()
	worker.calendarStreams = streams
}

func (worker *ServiceWorker) getCalendarInfo() StreamList {
	glog.Infoln("ServiceWorker.getCalendarInfo")
	worker.mu.RLock()
	defer worker.mu.RUnlock()
	res := make(StreamList, len(worker.calendarStreams))
	copy(res, worker.calendarStreams)
	return res
}

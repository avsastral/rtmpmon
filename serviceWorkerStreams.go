package main

import (
	"fmt"
	"github.com/golang/glog"
	"time"
)

func (worker *ServiceWorker) runPullStreams(streams StreamList) {
	glog.Infoln("ServiceWorker.runPullStreams")
	if len(streams) == 0 {
		glog.Infoln("ServiceWorker.runPullStreams :: нет потоков для запуска")
		return
	}
	worker.app.pulls.mu.Lock()
	defer worker.app.pulls.mu.Unlock()
	for _, stream := range streams {
		// todo: разобраться с ovz!
		streamName := fmt.Sprintf("%s_%d", worker.app.cfg.ppe, stream.CamId)
		s := StreamDesc{
			Uri:        stream.PlayUrl,
			StreamName: streamName,
			Title:      stream.AudienceName,
		}
		if err := worker.app.pulls.addOne(s); err != nil {
			glog.Errorf("ServiceWorker.runPullStreams :: ошибка старта трансляции потока: %s\n", err)
			continue
		}
	}
}

func(worker *ServiceWorker) runRecStreams(streams StreamList) {
	glog.Infoln("ServiceWorker.runRecStreams")
	if len(streams) == 0 {
		glog.Infoln("ServiceWorker.runRecStreams :: нет потоков для запуска")
		return
	}
	worker.app.records.mu.Lock()
	defer worker.app.records.mu.Unlock()
	for _, stream := range streams {
		streamName := fmt.Sprintf("%s_%d", worker.app.cfg.ppe, stream.CamId)
		rec := Record {
			Name: streamName,
			SegmentSec: 1800,
			LimitSec: 0,
			UriRec: stream.RecUrl,
			Title: stream.AudienceName,
		}
		if err := worker.app.records.addOne(rec); err != nil {
			glog.Errorf("ServiceWorker.performCheckForAutoStartCams :: ошибка старта записи потока: %s\n", err)
		}
	}
}

func (worker *ServiceWorker) performUpdateCamsInfo() {
	glog.Infoln("ServiceWorker.performUpdateCamsInfo:: РЦОИ: ", worker.app.cfg.regionalInfProcessingCenter)
	curTime := time.Now()
	if !worker.app.cfg.regionalInfProcessingCenter {
		glog.Infoln("ServiceWorker.performUpdateCamsInfo:: не РЦОИ проверяем время для самоконтроля")
		// если не РЦОИ - тогда врубаем проверку по времени (а для РЦОИ - проверка 24 часа)
		if curTime.Hour() < 7 || curTime.Hour() >= 19 {
			// самоконтроль надо сделать только в период с 7:00 до 19:00
			glog.Infoln("ServiceWorker.performUpdateCamsInfo :: не РЦИО - самоконтроль осуществляется в период с 7 до 19")
			return
		}
	}

	examStreams := worker.getCalendarInfo()
	worker.app.examToday = len(examStreams) != 0
	if !worker.app.examToday {
		glog.Infoln("ServiceWorker.performUpdateCamsInfo :: сегодня нет экзамена - самоконтроль деактивирован")
		return
	}
	var turnedOffStreams StreamList
	turnedOffStreams = worker.app.sys.CheckStreams(examStreams)
	worker.runPullStreams(turnedOffStreams)

	turnedOffStreams = worker.app.sys.CheckRecords(examStreams)
	worker.runRecStreams(turnedOffStreams)
}
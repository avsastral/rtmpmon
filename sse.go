package main

import (
	"github.com/golang/glog"
	"sync"
	"time"
)

type Event struct {
	Name string
	Data interface{}
}

const sendTimeout = time.Millisecond * 500
const sendPeriod = time.Millisecond * 1300
const sendBatchLimit = 20

type EventStream chan Event

//type ConnectionCallback func(EventStream)

type Greeter interface {
	Greet(EventStream)
}

type Broker struct {
	// Events are pushed to this interface{channel by the main events-gathering routine
	Events EventStream
	// New client connections
	newClients chan EventStream
	// Closed client connections
	closingClients chan EventStream
	// Client connections registry
	clients map[EventStream]bool

	// service fields
	batchOfRecEvents  map[string]Record
	batchOfPullEvents map[string]StreamDesc
	batchOfRecTimer   *time.Timer
	batchOfPullTimer  *time.Timer
}

func NewBroker(grs ...Greeter) (b *Broker) {
	glog.Infoln("Broker.NewBroker")
	// Instantiate a b
	b = &Broker{
		Events:            make(EventStream),
		newClients:        make(chan EventStream, 10),
		closingClients:    make(chan EventStream),
		clients:           make(map[EventStream]bool),
		batchOfRecEvents:  make(map[string]Record),
		batchOfPullEvents: make(map[string]StreamDesc),
	}

	// Set it running - listening and broadcasting events
	go b.listenSynchronously(grs)
	return
}

func (b *Broker) listen(grs []Greeter) {
	glog.Infoln("Broker.listen")
	for {
		select {
		case s := <-b.newClients:
			// A new client has connected.
			b.greetNewClient(s, grs)
		case cs := <-b.closingClients:
			// A client has detached and we want to
			// stop sending them messages.
			delete(b.clients, cs)
			close(cs)
			glog.Infof("Broker.listen: Client Removed. %d registered clients", len(b.clients))
		case event := <-b.Events:
			// We got a new event from the outside!
			// Send event to all connected clients
			b.sendEvent(event)
			//switch event.Name {
			//case "system":
			//	// Системные оправляем сразу
			//	glog.Infof("Broker.listen: Events :: system -> send immediately")
			//	b.sendEvent(event)
			//case "pull":
			//	b.processPullEvent(event)
			//case "rec":
			//	b.processRecEvent(event)
			//default:
			//	// всякое остальное если есть оправляем сразу
			//	b.sendEvent(event)
			//}
		}
	}
}

func (b *Broker) processRecEvent(event Event) {
	// rec оправляем буферизовано
	if b.batchOfRecTimer != nil {
		b.batchOfRecTimer.Stop()
	}
	portionOfEventData := event.Data.(map[string]Record)
	for k, v := range portionOfEventData {
		b.batchOfRecEvents[k] = v
	}
	if len(b.batchOfRecEvents) > sendBatchLimit {
		glog.Infof("Broker.listen: Events :: rec -> send batch by count")
		b.sendEvent(Event{Name: "rec", Data: b.batchOfRecEvents})
		b.batchOfRecEvents = make(map[string]Record)
		return
	}
	b.batchOfRecTimer = time.NewTimer(sendPeriod)
	go func() {
		<-b.batchOfRecTimer.C
		glog.Infof("Broker.listen: Events :: rec -> timer to send")
		if len(b.batchOfRecEvents) > 0 {
			glog.Infof("Broker.listen: Events :: rec -> send batch by timer")
			b.sendEvent(Event{Name: "rec", Data: b.batchOfRecEvents})
			b.batchOfRecEvents = make(map[string]Record)
		}
	}()
}

func (b *Broker) processPullEvent(event Event) {
	// pull оправляем буферизовано
	if b.batchOfPullTimer != nil {
		b.batchOfPullTimer.Stop()
	}
	portionOfEventData := event.Data.(map[string]StreamDesc)
	for k, v := range portionOfEventData {
		b.batchOfPullEvents[k] = v
	}
	if len(b.batchOfPullEvents) > sendBatchLimit {
		glog.Infof("Broker.listen: Events :: pull -> send batch by count")
		b.sendEvent(Event{Name: "pull", Data: b.batchOfPullEvents})
		b.batchOfPullEvents = make(map[string]StreamDesc)
		return
	}
	b.batchOfPullTimer = time.NewTimer(sendPeriod)
	go func() {
		<-b.batchOfPullTimer.C
		glog.Infof("Broker.listen: Events :: pull -> timer to send")
		if len(b.batchOfPullEvents) > 0 {
			glog.Infof("Broker.listen: Events :: pull -> send batch by timer")
			b.sendEvent(Event{Name: "pull", Data: b.batchOfPullEvents})
			b.batchOfPullEvents = make(map[string]StreamDesc)
		}
	}()
}

func (b *Broker) greetNewClient(clientStream EventStream, grs []Greeter) {
	glog.Infoln("Broker.greetNewClient")
	defer func() {
		if r := recover(); r != nil {
			glog.Errorln("Broker.greetNewClient :: Panic Recovered: ", r)
		}
	}()
	// Register their message channel
	b.clients[clientStream] = true
	// опасность если мы еще не поприветствовали товарища - а он уже свалил в туман -
	// будет фаталити с отправкой в закрытый канал; поэтому рекаверимся в этой функции
	for _, g := range grs {
		go g.Greet(clientStream)
	}
	// A new client has connected.
	glog.Infof("Broker.listen: Client added. %d registered clients", len(b.clients))
}

func (b *Broker) sendEvent(event Event) {
	glog.Infoln("Broker.sendEvent to all connected clients")
	var wg sync.WaitGroup
	for clientMessageChan := range b.clients {
		// в любом случае отправляе в го-рутинах (спорноый вопрос - нужен ли тут waitGroup?
		wg.Add(1)
		go func() {
			defer wg.Done()
			// отправлка с таймаутом - если канал текущего клиента по каким-то причинам недоступен - то пропускаем
			select {
			case clientMessageChan <- event:
			case <-time.After(sendTimeout):
				glog.Infoln("Broker.sendEvent: Send Event Timeout - skipping client")
			}
		}()
	}
	wg.Wait()
	glog.Infoln("Broker.sendEvent to all connected clients - done")
}

func (b *Broker) listenSynchronously(grs []Greeter) {
	glog.Infoln("Broker.listenSynchronously")
	for {
		select {
		case s := <-b.newClients:
			// A new client has connected.
			// Register their message channel
			b.greetNewClient(s, grs)
			//b.clients[s] = true
			//for _, g := range grs {
			//	go g.Greet(s)
			//}
			//// A new client has connected.
			//glog.Infof("Broker.listen: Client added. %d registered clients", len(b.clients))
		case cs := <-b.closingClients:
			// A client has detached and we want to
			// stop sending them messages.
			delete(b.clients, cs)
			close(cs)
			glog.Infof("Broker.listen: Client Removed. %d registered clients", len(b.clients))
		case event := <-b.Events:
			// We got a new event from the outside!
			// Send event to all connected clients
			//b.sendEvent(event)
			for clientMessageChan := range b.clients {
				select {
				case clientMessageChan <- event:
					//glog.Infoln("Broker.sendEvent: Send Event sent successfully")
					continue
				case <-time.After(sendTimeout):
					glog.Infoln("Broker.sendEvent: Send Event Timeout - skipping client")
				}
				//clientMessageChan <- event
			}
		}
	}
}
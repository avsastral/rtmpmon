package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"net/http"
)

func (b *Broker) SSE(c context.Context, rw http.ResponseWriter, req *http.Request) error {
	glog.Infoln("Broker.SSE")
	// Make sure that the writer supports flushing.
	flusher, ok := rw.(http.Flusher)

	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		glog.Errorf("Broker.SSE :: error Streaming unsupported")
		return errors.New("Streaming unsupported")
	}

	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("X-Accel-Buffering", "no")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	// Each connection registers its own message channel with the Broker's connections registry
	messageChan := make(EventStream)

	// Signal the b that we have a new connection
	b.newClients <- messageChan

	// Remove this client from the map of connected clients
	// when this handler exits.
	defer func() {
		b.closingClients <- messageChan
	}()

	// Listen to connection close and un-register messageChan
	notify := rw.(http.CloseNotifier).CloseNotify()
	enc := json.NewEncoder(rw)

	for {
		select {
		case <-notify:
			return nil
		case msg := <-messageChan:
			// Write to the ResponseWriter
			// Server Sent Events compatible
			fmt.Fprintf(rw, "event: %s\n", msg.Name)
			fmt.Fprintf(rw, "data: ")
			enc.Encode(msg.Data)
			fmt.Fprint(rw, "\n\n")

			// Flush the data immediatly instead of buffering it for later.
			flusher.Flush()
		}
	}
}

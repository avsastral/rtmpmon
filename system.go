package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/dustin/go-humanize"
	"golang.org/x/net/context"
)

const (
	maxMeasureChunk  = 1024 * 1024
	measureInterval  = 10 * time.Second
	measureTimeout   = 4 * time.Second
	measurementSteps = 4
	portCheckTimeout = 1 * time.Second
)

type ClientInfo struct {
	Name               string         `json:"name"`
	AvsVersion         string         `json:"avsVersion"`
	FreeSpace          uint64         `json:"freeSpace"`
	OutBps             uint64         `json:"outBps,omitempty"`
	HasInet            bool           `json:"hasInet"`
	PushPortsAvailable bool           `json:"pushPortsAvailable"`
	Cams               Cams           `json:"cams"`
	CamsRecordStats    CamsRecordInfo `json:"cams_record_stats"`
	CamsRelayStats     CamsRelayInfo  `json:"cams_relay_stats"`
	CamList            CamList        `json:"cam_list,omitempty"`
}

type CalendarInfo struct {
	ExamToday bool `json:"exam_today"`
}

type SysStats struct {
	app    *App
	Info   ClientInfo
	cancel func()
	Locker
}

type CamsRelayInfo struct {
	TurnedOff  []string `json:"turned_off"`
	Restarting []string `json:"restarting"`
}

type CamsRecordFilesInfo struct {
	Audience   string `json:"audience"`
	Reason     string `json:"reason"`
	ReasonCode int    `json:"-"`
}

type CamsRecordInfo struct {
	CamsRelayInfo
	FilesInfo []CamsRecordFilesInfo `json:"files_info"`
}

type Cams struct {
	Total        int `json:"cam_all"`
	RecOn        int `json:"cam_rec_on"`
	RecOff       int `json:"cam_rec_off"`
	RecWaiting   int `json:"cam_rec_waiting"`
	ProxyOn      int `json:"cam_proxy_on"`
	ProxyOff     int `json:"cam_proxy_off"`
	ProxyWaiting int `json:"cam_proxy_waiting"`
}

type CamList map[string]CamDesc

type CamDesc struct {
	Name      string `json:"name"`
	Comment   string `json:"comment"`
	RemoteUrl string `json:"remoteUrl,omitempty"`
	PlayUrl   string `json:"playUrl,omitempty"`
	RecUrl    string `json:"recUrl,omitempty"`
	Pull      string `json:"pull"`
	Push      string `json:"push"`
	Rec       string `json:"rec"`
}

type BwMeasurement struct {
	NumBytes     int `json:"numBytes"`
	Milliseconds int `json:"milliseconds"`
}

func (stats *SysStats) Init(app *App) {
	glog.Infoln("SysStats.Init")
	stats.app = app
	app.sys = stats
	ctx, cancel := context.WithCancel(context.Background())
	stats.cancel = cancel
	stats.Info.Name = app.cfg.ppe
	stats.Info.AvsVersion = version
	app.OnClose(stats.Close)

	go stats.PeriodicCheck(ctx)
	if stats.app.cfg.monitorServer == "" {
		return
	}
	go stats.PeriodicUpload(ctx)
}

func (stats *SysStats) Close() error {
	glog.Infoln("SysStats.Close")
	// а пока просто остановим наши го-рутины
	stats.cancel()
	return nil
}

func (stats *SysStats) updateInternetAccess() {
	//glog.Infoln("SysStats.updateInternetAccess")
	destList := []string{
		fmt.Sprintf("http://monitoring:Vjybnjhbyu@%s/monitoring/sys/", stats.app.cfg.publishServer),
		fmt.Sprintf("https://%s/api/calendar/list", stats.app.cfg.avsMonitoringServer),
	}

	var urlRes = make([]bool, len(destList))
	var wg sync.WaitGroup
	for i, dest := range destList {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			urlRes[index] = CheckUrlAvailability(dest)
		}(i)
	}
	wg.Wait()
	stats.Info.HasInet = true
	for i := range urlRes {
		if !urlRes[i] {
			stats.Info.HasInet = false
			return
		}
	}
}

func (stats *SysStats) updatePushPortsAccess() {
	//glog.Infoln("SysStats.updatePushPortsAccess")
	// todo: сделать по кнопке - ибо постоянно теребонькать - это серверу плохо становится
	stats.Info.PushPortsAvailable = true
	return
	//var portRes = make([]bool, len(PushPorts))
	//var wg sync.WaitGroup
	//for i, port := range PushPorts {
	//	wg.Add(1)
	//	go func(index int) {
	//		defer wg.Done()
	//		portRes[index] = CheckPortAvailability(stats.app.cfg.remotePublishServer, port)
	//	}(i)
	//}
	//wg.Wait()
	//stats.Info.PushPortsAvailable = true
	//for i := range portRes {
	//	if !portRes[i] {
	//		stats.Info.PushPortsAvailable = false
	//		return
	//	}
	//}
}

func (stats *SysStats) updateBandwidth() error {
	if stats.app.cfg.measureServer == "none" {
		return nil
	}
	//glog.Infoln("SysStats.updateBandwidth")

	dest := fmt.Sprintf("http://monitoring:Vjybnjhbyu@%s/monitoring/sys/bw", stats.app.cfg.measureServer)
	b := make([]byte, maxMeasureChunk/2)
	if _, err := rand.Read(b); err != nil {
		return err
	}
	client := http.Client{Timeout: measureTimeout}
	payload := bytes.NewReader(b)
	rsp, err := client.Post(dest, "application/binary", payload)
	if err != nil {
		return err
	}
	defer rsp.Body.Close()

	var meas BwMeasurement
	err = json.NewDecoder(rsp.Body).Decode(&meas)
	if err != nil || meas.Milliseconds == 0 {
		return err
	}

	bps := uint64((1000 * meas.NumBytes) / meas.Milliseconds)
	if stats.Info.OutBps == 0 {
		stats.Info.OutBps = bps
	} else {
		stats.Info.OutBps = (stats.Info.OutBps*(measurementSteps-1) + bps) / measurementSteps
	}
	glog.Infoln("SysStats.updateBandwidth: Bandwidth: %s/s (%s/s average)", humanize.IBytes(bps), humanize.IBytes(stats.Info.OutBps))
	return nil
}

func (stats *SysStats) updateSpace() error {
	//glog.Infoln("SysStats.updateSpace")
	var s syscall.Statfs_t
	if err := syscall.Statfs(stats.app.mediaDir, &s); err != nil {
		return err
	}
	// Available blocks * size per block = available space in bytes
	stats.Info.FreeSpace = s.Bavail * uint64(s.Bsize)
	return nil
}

func (stats *SysStats) Camera(camId string) (res CamDesc, err error) {
	glog.Infoln("SysStats.Camera")
	res = CamDesc{}
	err = stats.app.db.QueryRow(`select
		coalesce(name, '') as name,
		coalesce(comment, '') as comment,
		coalesce(ip_view, '') as ip_view,
		coalesce(ip_rec, '') as ip_rec
		from cms_cam
		where id = ?`, camId).Scan(&res.Name, &res.Comment, &res.PlayUrl, &res.RecUrl)

	if err != nil {
		glog.Errorf("SysStats.Camera : Error query camera %s: %v", camId, err)
	}
	return res, err
}

func (stats *SysStats) Cameras() (res CamList) {
	glog.Infoln("SysStats.Cameras")
	res = make(CamList)
	db := stats.app.db

	rows, err := db.Query(`select
		id, coalesce(name, '') as name,
		coalesce(comment, '') as comment,
		coalesce(ip_view, '') as ip_view,
		coalesce(ip_rec, '') as ip_rec
	from cms_cam`)
	if err != nil {
		glog.Errorf("SysStats.Cameras: Error accessing database: %v", err)
		return
	}
	defer rows.Close()

	var temp CamDesc
	for rows.Next() {
		var (
			id         int
			remoteName string
		)
		err = rows.Scan(&id, &temp.Name, &temp.Comment, &temp.PlayUrl, &temp.RecUrl)
		if err != nil {
			glog.Errorf("SysStats.Cameras: Error scanning database: %v", err)
			return
		}
		streamName := fmt.Sprintf("%s_%d", stats.app.cfg.ppe, id)
		if stats.app.cfg.newStreamNames && checkDescRe.MatchString(temp.Comment) {
			remoteName = temp.Comment
		} else {
			remoteName = streamName
		}
		temp.RemoteUrl = UrlJoin(stats.app.relay.Route(remoteName), remoteName)
		res[streamName] = temp
	}
	if err = rows.Err(); err != nil {
		glog.Errorf("SysStats.Cameras: Error after scanning database: %v", err)
	}
	return
}

func (stats *SysStats) checkRecordsFiles(calendarStreams StreamList) (res []CamsRecordFilesInfo, streamsWithNoFiles StreamList) {
	_, offset := time.Now().Zone()
	now := time.Now().Local()
	glog.Infoln("SysStats.CheckRecords for: ", now.Format(RecDateFmt+" "+RecTimeFmt))
	res = []CamsRecordFilesInfo{}
	streamsWithNoFiles = make(StreamList, 0)
	yearDir := fmt.Sprintf("%d", now.Year())
	dayDir := now.Format(RecDateFmt)
	var from, to time.Time
	if now.Minute() < 30 {
		from = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, now.Location())
		to = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 30, 0, 0, now.Location())
	} else {
		from = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 30, 0, 0, now.Location())
		to = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 59, 59, 0, now.Location())
	}
	for _, stream := range calendarStreams {
		recName := fmt.Sprintf("%s_%d", stats.app.cfg.ppe, stream.CamId)
		recAudience, err := stats.app.records.getAudience(recName)
		if err != nil {
			glog.Errorf("SysStats.checkRecordsFiles: Error getting audience name: %s", err)
			res = append(res, CamsRecordFilesInfo{
				Audience:   stream.AudienceCode,
				Reason:     "Ошибка чтения директории",
				ReasonCode: BadRecErrorReadDir,
			})
			continue
		}
		audienceDir := filepath.Join(stats.app.mediaDir, yearDir, dayDir, recAudience)
		var streamFiles []time.Time
		err = filepath.Walk(audienceDir, func(path string, info os.FileInfo, err error) error {
			if err != nil || info.IsDir() || !strings.HasSuffix(path, ".mp4") {
				return nil
			}
			components := strings.Split(path, "/")
			n := len(components)
			d, err := time.Parse(RecDateFmt+" "+RecTimeFmt, dayDir+" "+strings.TrimSuffix(components[n-1], ".mp4"))
			if err != nil {
				glog.Warningf("SysStats.checkRecordsFiles.Walk: Failed to parse date for %s, error %v", path, err)
				return nil
			}
			d = d.Add(time.Duration(-offset) * time.Second)
			if (d.Before(from)) || (d.After(to)) {
				return nil
			}
			streamFiles = append(streamFiles, d)
			return nil
		})
		if err != nil {
			glog.Warningf("SysStats.checkRecordsFiles.Walk: error scanning %s, error %v", audienceDir, err)
		}
		// проверим что у нас среди найденных файлов записей - должна быть строго 1 запись и минуты должны быть либо 0 либо 30
		if len(streamFiles) != 1 {
			glog.Warningf("SysStats.checkRecordsFiles: Problems with recording files for camera: %s", stream.AudienceCode)
			reason := "Отсутствует запись за текущий период"
			reasonCode := BadRecNoFile
			if len(streamFiles) > 1 {
				reasonCode = BadRecManyFiles
				if len(streamFiles) > 4 {
					reason = fmt.Sprintf("%d файлов за тек. период", len(streamFiles))
				} else {
					reason = fmt.Sprintf("%d файла за тек. период", len(streamFiles))
				}
				//reason = "Несколько файлов записей"
			}
			res = append(res, CamsRecordFilesInfo{
				Audience:   stream.AudienceCode,
				Reason:     reason,
				ReasonCode: reasonCode,
			})
			if reasonCode == BadRecNoFile {
				streamsWithNoFiles = append(streamsWithNoFiles, stream)
			}
		} else {
			if !(streamFiles[0].Minute() == 0 || streamFiles[0].Minute() == 30) {
				res = append(res, CamsRecordFilesInfo{
					Audience:   stream.AudienceCode,
					Reason:     "Неверное время старта записи",
					ReasonCode: BadRecIncorrectTime,
				})
			}
		}
	}
	return
}

func (stats *SysStats) CheckRecords(calendarStreams StreamList) (res StreamList) {
	// проверяет записи, и возвращает потоки, которые должны записывать но не записывают - вышестоящий код разберется что делать
	glog.Infoln("SysStats.CheckRecords")
	res = make(StreamList, 0)
	var turnedOffCams []string
	var restartingCams []string
	func() {
		stats.app.records.mu.RLock()
		defer stats.app.records.mu.RUnlock()

		for _, stream := range calendarStreams {
			streamName := fmt.Sprintf("%s_%d", stats.app.cfg.ppe, stream.CamId)
			if recStream, ok := stats.app.records.records[streamName]; ok {
				if recStream.Process == nil {
					turnedOffCams = append(turnedOffCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckRecords : there is no rec stream for: %s", streamName)
					res = append(res, stream)
					continue
				}
				switch recStream.Process.State {
				case "RUNNING":
					glog.Infof("SysStats.CheckRecords : rec stream for: %s works fine", streamName)
				case "STARTING", "BACKOFF", "RESTARTING", "STOPPING", "KILLING", "KILLINGBYPATTERN":
					restartingCams = append(restartingCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckRecords : rec stream for: %s is restarting", streamName)
				default:
					turnedOffCams = append(turnedOffCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckRecords : there is no rec stream for: %s", streamName)
					res = append(res, stream)
				}
			} else {
				turnedOffCams = append(turnedOffCams, stream.AudienceCode)
				glog.Warningf("SysStats.CheckRecords : there is no rec stream for: %s", streamName)
				res = append(res, stream)
			}
		}
	}()
	recordsInfo, noFilesStreams := stats.checkRecordsFiles(calendarStreams)
	for _, noFileStream := range noFilesStreams {
		res = append(res, noFileStream)
	}

	stats.mu.Lock()
	defer stats.mu.Unlock()
	stats.Info.CamsRecordStats.Restarting = restartingCams
	stats.Info.CamsRecordStats.TurnedOff = turnedOffCams
	stats.Info.CamsRecordStats.FilesInfo = recordsInfo
	return res
}

func (stats *SysStats) CheckStreams(calendarStreams StreamList) (res StreamList) {
	// проверяет вещание, и возвращает потоки, которые должны вещать но не вещают - вышестоящий код разберется что делать
	glog.Infoln("SysStats.CheckStreams")
	res = make(StreamList, 0)
	var turnedOffCams []string
	var restartingCams []string
	func() {
		stats.app.pulls.mu.RLock()
		defer stats.app.pulls.mu.RUnlock()

		for _, stream := range calendarStreams {
			streamName := fmt.Sprintf("%s_%d", stats.app.cfg.ppe, stream.CamId)
			if pullStream, ok := stats.app.pulls.streams[streamName]; ok {
				if pullStream.relay == nil {
					turnedOffCams = append(turnedOffCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckStreams : there is no push stream for: %s", streamName)
					res = append(res, stream)
					continue
				}
				switch pullStream.relay.State {
				//  IDLE
				//  RUNNING
				//	STARTING
				//	STOPPING
				//	RESTARTING
				//	KILLING
				//	KILLINGBYPATTERN
				//	BACKOFF
				case "RUNNING":
					glog.Infof("SysStats.CheckStreams : push stream for: %s works fine", streamName)
				case "STARTING", "BACKOFF", "RESTARTING", "STOPPING", "KILLING", "KILLINGBYPATTERN":
					restartingCams = append(restartingCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckStreams : push stream for: %s is restarting", streamName)
				default:
					turnedOffCams = append(turnedOffCams, stream.AudienceCode)
					glog.Warningf("SysStats.CheckStreams : there is no push stream for: %s", streamName)
					res = append(res, stream)
				}
			} else {
				turnedOffCams = append(turnedOffCams, stream.AudienceCode)
				glog.Warningf("SysStats.CheckStreams : there is no push stream for: %s", streamName)
				res = append(res, stream)
			}
		}
	}()
	stats.mu.Lock()
	defer stats.mu.Unlock()
	stats.Info.CamsRelayStats.Restarting = restartingCams
	stats.Info.CamsRelayStats.TurnedOff = turnedOffCams
	return res
}

func (stats *SysStats) PeriodicCheck(c context.Context) {
	//glog.Infoln("SysStats.PeriodicCheck")
	<-stats.app.ready

	for {
		func() {
			stats.mu.Lock()
			defer stats.mu.Unlock()
			if err := stats.updateSpace(); err != nil {
				glog.Errorf("SysStats.PeriodicCheck: Error updating free space", err)
			}
			if err := stats.updateBandwidth(); err != nil {
				glog.Errorf("SysStats.PeriodicCheck: Error updating bandwidth", err)
			}
			stats.updateInternetAccess()
			stats.updatePushPortsAccess()
			// Send all
			stats.app.bus.Events <- Event{"system", stats.Info}
		}()
		select {
		case <-c.Done():
			return
		case <-time.After(measureInterval):
		}
	}
}

func (stats *SysStats) calcTotals(camList CamList) (res Cams) {
	glog.Infoln("SysStats.calcTotals")
	res.Total = len(camList)
	res.ProxyOff = res.Total
	res.RecOff = res.Total
	func() {
		stats.app.pulls.mu.RLock()
		defer stats.app.pulls.mu.RUnlock()

		for key, cam := range stats.app.pulls.streams {
			if cam.Process == nil {
				continue
			}
			desc := camList[key]
			desc.Pull = cam.Process.State
			if cam.relay != nil {
				desc.Push = cam.relay.State
				switch cam.relay.State {
				case "RUNNING":
					res.ProxyOn++
					res.ProxyOff--
				case "STARTING", "BACKOFF", "RESTARTING", "STOPPING", "KILLING", "KILLINGBYPATTERN":
					res.ProxyWaiting++
					res.ProxyOff--
				}
			}
			camList[key] = desc
		}
	}()
	func() {
		stats.app.records.mu.RLock()
		defer stats.app.records.mu.RUnlock()

		for key, cam := range stats.app.records.records {
			if cam.Process == nil {
				continue
			}
			switch cam.Process.State {
			case "RUNNING":
				res.RecOn++
				res.RecOff--
			case "STARTING", "BACKOFF", "RESTARTING", "STOPPING", "KILLING", "KILLINGBYPATTERN":
				res.RecWaiting++
				res.RecOff--
			}
			desc := camList[key]
			desc.Rec = cam.Process.State
			camList[key] = desc
		}
	}()
	return
}

// специфично для Тулы
func (stats *SysStats) sendMonitoringState() error {
	glog.Infoln("SysStats.sendMonitoringState")
	stats.Locker.mu.RLock()
	defer stats.Locker.mu.RUnlock()

	uri := stats.app.cfg.monitorServer + "/school.php"
	res := stats.Info
	res.CamList = stats.Cameras()
	res.Cams = stats.calcTotals(res.CamList)
	buf := new(bytes.Buffer)
	if err := json.NewEncoder(buf).Encode(res); err != nil {
		return err
	}
	client := &http.Client{Timeout: 2 * time.Second}
	if _, err := client.Post(uri, "application/json", buf); err != nil {
		return err
	}
	glog.Infoln("SysStats.sendMonitoringState: Sent monitoring data to ", uri)
	return nil
}

func (stats *SysStats) PeriodicUpload(ctx context.Context) {
	<-stats.app.ready
	glog.Infoln("SysStats.PeriodicUpload")

	for {
		if err := stats.sendMonitoringState(); err != nil {
			glog.Errorf("SysStats.PeriodicUpload: Error sending monitoring state: %v", err)
		}
		select {
		case <-ctx.Done():
			return
		case <-time.After(measureInterval):
		}
	}
}

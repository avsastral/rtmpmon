package main

import (
	"fmt"
	"github.com/andviro/noodle/render"
	"github.com/golang/glog"
	"golang.org/x/net/context"
	"io/ioutil"
	"net/http"
	"time"
)

func (stats *SysStats) Get(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("SysStats.Get")
	res := stats.Info
	res.CamList = stats.Cameras()
	res.Cams = stats.calcTotals(res.CamList)
	return render.Yield(c, 200, res)
}

func (stats *SysStats) GetExam(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("SysStats.GetExam")
	res := CalendarInfo{
		ExamToday: stats.app.examToday,
	}
	return render.Yield(c, 200, res)
}

func (stats *SysStats) MeasureBandwidth(c context.Context, w http.ResponseWriter, r *http.Request) error {
	glog.Infoln("SysStats.MeasureBandwidth")
	if r.ContentLength > maxMeasureChunk {
		return fmt.Errorf("bandwidth chunk too big: %d", r.ContentLength)
	}
	startTime := time.Now()
	b, err := ioutil.ReadAll(r.Body)
	delta := time.Since(startTime)
	if err != nil {
		return fmt.Errorf("error reading bandwidth chunk: %v", err)
	}
	if err := r.Body.Close(); err != nil {
		glog.Errorf("SysStats.MeasureBandwidth: Error closing request")
	}
	res := BwMeasurement{NumBytes: len(b), Milliseconds: int(delta / time.Millisecond)}
	return render.Yield(c, 200, &res)
}

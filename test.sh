#!/bin/sh
curl -v \
    -s \
    -H "Content-Type: application/json" \
    -X POST \
    -d '{
            "uri": "rtsp://localhost:5554/vod/test",
            "streamName": "cam1"
    }'\
    http://localhost:65088/pull

ffprobe rtmp://localhost:1935/vod/cam1
sleep 20

curl -v \
    -s \
    -X DELETE \
    http://localhost:65088/pull/cam1

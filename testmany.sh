#!/bin/sh
curl -v \
    -s \
    -H "Content-Type: application/json" \
    -X PUT \
    -d '[{
            "uri": "rtsp://localhost:5554/vod/test",
            "streamName": "cam1"
    },{
            "uri": "rtsp://localhost:5554/vod/test",
            "streamName": "cam2"
    }
    ]'\
    http://localhost:5005/pull

#ffprobe rtmp://localhost:1935/vod/cam1
sleep 5

curl -v \
    -s \
    -X DELETE \
    -d '["cam1", "cam2"]' \
    http://localhost:5005/pull

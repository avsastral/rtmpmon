#!/bin/sh
curl -v \
    -s \
    -H "Content-Type: application/json" \
    -X PUT \
    -d '[{
            "targetURI": "rtsp://localhost:5554/",
            "localStreamName": "test",
            "targetStreamName": "rtsp_test2"
    }]'\
    http://localhost:5005/push

sleep 5
#ffprobe rtmp://localhost:1935/vod/rtsp_test2

curl -v \
    -s \
    -X DELETE \
    -d '["test"]'\
    http://localhost:5005/push

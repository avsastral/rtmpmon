#!/bin/sh
echo "************************************"
curl -v \
    -s \
    -H "Content-Type: application/json" \
    -X POST \
    -d '{
            "streamName": "test",
            "limitSec": 30
    }'\
    http://localhost:65088/rec

sleep 3

echo "************************************"
curl -v \
    -s \
    http://localhost:65088/rec

sleep 15

echo "************************************"
curl -v \
    -s \
    -X DELETE \
    http://localhost:65088/rec/test

// Code generated by protoc-gen-go.
// source: simple_user.proto
// DO NOT EDIT!

/*
Package protobuf is a generated protocol buffer package.

It is generated from these files:
	simple_user.proto

It has these top-level messages:
	SimpleUser
*/
package protobuf

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
const _ = proto.ProtoPackageIsVersion1

type SimpleUser struct {
	Id               *uint64 `protobuf:"varint,1,req,name=id" json:"id,omitempty"`
	Name             *string `protobuf:"bytes,2,req,name=name" json:"name,omitempty"`
	Age              *int32  `protobuf:"varint,3,opt,name=age,def=0" json:"age,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *SimpleUser) Reset()                    { *m = SimpleUser{} }
func (m *SimpleUser) String() string            { return proto.CompactTextString(m) }
func (*SimpleUser) ProtoMessage()               {}
func (*SimpleUser) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

const Default_SimpleUser_Age int32 = 0

func (m *SimpleUser) GetId() uint64 {
	if m != nil && m.Id != nil {
		return *m.Id
	}
	return 0
}

func (m *SimpleUser) GetName() string {
	if m != nil && m.Name != nil {
		return *m.Name
	}
	return ""
}

func (m *SimpleUser) GetAge() int32 {
	if m != nil && m.Age != nil {
		return *m.Age
	}
	return Default_SimpleUser_Age
}

func init() {
	proto.RegisterType((*SimpleUser)(nil), "protobuf.SimpleUser")
}

var fileDescriptor0 = []byte{
	// 100 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0xe2, 0x12, 0x2c, 0xce, 0xcc, 0x2d,
	0xc8, 0x49, 0x8d, 0x2f, 0x2d, 0x4e, 0x2d, 0xd2, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2, 0x00,
	0x53, 0x49, 0xa5, 0x69, 0x4a, 0x66, 0x5c, 0x5c, 0xc1, 0x60, 0xe9, 0x50, 0xa0, 0xac, 0x10, 0x17,
	0x17, 0x53, 0x66, 0x8a, 0x04, 0xa3, 0x02, 0x93, 0x06, 0x8b, 0x10, 0x0f, 0x17, 0x4b, 0x5e, 0x62,
	0x6e, 0xaa, 0x04, 0x13, 0x90, 0xc7, 0x29, 0xc4, 0xc7, 0xc5, 0x9c, 0x98, 0x9e, 0x2a, 0xc1, 0xac,
	0xc0, 0xa8, 0xc1, 0x6a, 0xc5, 0x68, 0x00, 0x08, 0x00, 0x00, 0xff, 0xff, 0x64, 0x21, 0x66, 0x4d,
	0x55, 0x00, 0x00, 0x00,
}
